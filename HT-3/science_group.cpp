// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#include "science_group.hpp"
void
Project::print()
{
    std::cout << "Project name: " << project_name << std::endl;
    std::cout << "Project cost: " << cost << std::endl;
}
SciGroup::SciGroup()
{
    this->grad_student_count = 0;
    this->employee_count = 0;
    this->projects = {};
}
SciGroup::SciGroup(std::string group_name, std::string group_leader)
{
    this->group_name = group_name;
    this->group_leader = group_leader;
    this->grad_student_count = 0;
    this->employee_count = 0;
    this->projects = {};
}
SciGroup::SciGroup(std::string group_name,
                   std::string group_leader,
                   uint32_t grad_student_count,
                   uint32_t employee_count)
{
    this->group_name = group_name;
    this->group_leader = group_leader;
    this->grad_student_count = grad_student_count;
    this->employee_count = employee_count;
    this->projects = {};
}
SciGroup::SciGroup(std::string group_name,
                   std::string group_leader,
                   uint32_t grad_student_count,
                   uint32_t employee_count,
                   std::vector<Project> projects)
{
    this->group_name = group_name;
    this->group_leader = group_leader;
    this->grad_student_count = grad_student_count;
    this->employee_count = employee_count;
    this->projects = projects;
}

void
SciGroup::print() const
{
    std::cout << "----  Science group  ----" << std::endl;
    std::cout << "| Name:   " << group_name << std::endl;
    std::cout << "| Leader: " << group_leader << std::endl;
    std::cout << "| Graduated student count: " << grad_student_count;
    std::cout << std::endl;
    std::cout << "| Employee count: " << employee_count << std::endl;
    if(projects.size() != 0) {
        std::cout << "| Projects:" << std::endl;
        for(Project i : projects) {
            std::cout << "|----" << std::endl;
            std::cout << "|    Name: " << i.project_name << std::endl;
            std::cout << "|    Cost: " << i.cost << std::endl;
        }
    }
    std::cout << "-----------END-----------" << std::endl;
}
void
SciGroup::print(std::ostream& out) const
{
    out << "SCI_GROUP" << std::endl;
    out << "group name:" << std::endl << group_name << std::endl;
    out << "group leader:" << std::endl << group_leader << std::endl;
    out << "graduated student count:" << std::endl
        << grad_student_count << std::endl;
    out << "employee count:" << std::endl << employee_count << std::endl;
    if(projects.size() != 0) {
        out << "PROJECTS" << std::endl;
        for(Project i : projects) {
            out << "project name:" << std::endl << i.project_name << std::endl;
            out << "cost:" << std::endl << i.cost << std::endl;
            out << "end item" << std::endl;
        }
        out << "END" << std::endl;
    }
    out << "END" << std::endl;
}

void
SciGroup::dbread(std::istream& in)
{
    std::string line;
    std::getline(in, line);
    while(line != "END") {
        if(line == "group name:") {
            std::getline(in, group_name);
        } else if(line == "group leader:") {
            std::getline(in, group_leader);
        } else if(line == "graduated student count:") {
            std::getline(in, line);
            grad_student_count = std::stoi(line);
        } else if(line == "employee count:") {
            std::getline(in, line);
            employee_count = std::stoi(line);
        } else if(line == "PROJECTS") {
            Project buffer = { "buffer", 0 };
            while(line != "END") {
                if(line == "project name:") {
                    std::getline(in, buffer.project_name);
                } else if(line == "cost:") {
                    std::getline(in, line);
                    buffer.cost = std::stoi(line);
                } else if(line == "end item") {
                    projects.push_back(buffer);
                }
                std::getline(in, line);
            }
        }
        std::getline(in, line);
    }
}
void
SciGroup::setup_projects(std::string& line, std::vector<std::string>& PS1)
{
    PS1.push_back("<projects>");
    // std::string line = "help";
    Project buffer = { "null", 0 };
    while(line != "end") {
        if(line == "push") {
            projects.push_back(buffer);
            print();
            std::cout << PS1_str(PS1) << "Enter an option (or 'help')"
                      << std::endl;
        } else if(line == "pop") {
            projects.pop_back();
            print();
            std::cout << PS1_str(PS1) << "Enter an option (or 'help')"
                      << std::endl;
        } else {
            if(line == "name") {
                std::cout << "Enter project name: ";
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(),
                                '\n');
                std::getline(std::cin, buffer.project_name);
            } else if(line == "cost") {
                std::cout << "Enter project cost: ";
                std::cin >> buffer.cost;
            } else if(line == "help") {
                std::cout << SCI_PROJECTS_HELP << std::endl;
            } else {
                std::cout
                  << line
                  << ": option not found. Enter 'help' to view all available"
                  << std::endl;
            }
            std::cout << PS1_str(PS1) << "Buffered project:" << std::endl;
            buffer.print();
        }
        std::cin >> line;
    }
    PS1.pop_back();
}

bool
SciGroup::is_selected(uint64_t comp, uint64_t number)
{
    if(comp == 0) {
        return grad_student_count > number;
    } else if(comp == 1 && employee_count != 0) {
        return (grad_student_count / employee_count) >= number;
        // Yep, here we get integer division, because we are counting people
    }
    return false;
}

void
SciGroup::set_fields(std::string& line, std::vector<std::string>& PS1)
{
    PS1.push_back("<fields>");
    //    std::string line = "help";
    while(line != "END") {
        if(line == "name") {
            std::cout << "Enter group name: ";
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::getline(std::cin, group_name);
        } else if(line == "leader") {
            std::cout << "Enter group leader: ";
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::getline(std::cin, group_leader);
        } else if(line == "gsc") {
            std::cout << "Enter amount of graduated studens: ";
            std::cin >> grad_student_count;
        } else if(line == "emc") {
            std::cout << "Enter employee count: ";
            std::cin >> employee_count;
        } else if(line == "projects") {
            std::cout << "Enter projects:" << std::endl;
            setup_projects(line, PS1);
        } else {
            std::cout
              << line
              << ": option not found. Enter 'help' to view all available"
              << std::endl;
        }
        print();
        if(line == "help") {
            std::cout << SCI_HELP << std::endl;
        }
        std::cout << PS1_str(PS1) << "Enter an option (or 'help')" << std::endl;
        std::cin >> line;
    }
    PS1.pop_back();
}
uint64_t
SciGroup::cost() const
{
    if(!projects.empty()) {
        uint64_t sum = 0;
        for(Project i : projects) {
            sum += i.cost;
        }
        return sum;
    }
    return 0;
}
SciGroup::~SciGroup()
{
}