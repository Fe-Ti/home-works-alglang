// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#include "group.hpp"

void
Group::print() const
{
}
void
Group::print(std::ostream& out) const
{
}

void
Group::dbread(std::istream& in)
{
}

bool
Group::is_selected(uint64_t comp, uint64_t number)
{
    return 0;
}

void
Group::set_fields(std::string& line, std::vector<std::string>& PS1)
{
}

uint64_t
Group::cost() const
{
    return 0;
}

Group::~Group()
{
}

std::string
PS1_str(std::vector<std::string>& PS1)
{
    std::string buffer = "----\n" + PS1[0];
    for(uint64_t i = 1; i < PS1.size(); ++i) {
        buffer += "/" + PS1[i];
    }
    return buffer + "\n";
}
/*void
get_word(std::string& word, const std::string& prompt)
{
    std::cout << prompt << ":";
    std::cin >> word;
}*/
/*
template<typename T>
bool
compare(T val0, std::string& comp, T val1)
{
    if(comp == "=" || comp == "==")
        return val0 == val1;
    else if(comp == "<")
        return val0 < val1;
    else if(comp == ">")
        return val0 > val1;
    else if(comp == "=<" || comp == "<=")
        return val0 <= val1;
    else if(comp == ">=" || comp == "=>")
        return val0 >= val1;
    else
        throw compare_error("Can't compare via " + comp + "operator");
}*/