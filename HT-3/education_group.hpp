// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#ifndef EDU_GROUP
#define EDU_GROUP

#include "group.hpp"

struct Course {
    uint64_t student_count;
    uint64_t cost_per_student;
    void print();
};

class EduGroup : public Group
{
  public:
    std::vector<Course> courses;

    EduGroup();
    EduGroup(std::string group_name, std::string group_leader);
    EduGroup(std::string group_name,
             std::string group_leader,
             uint32_t grad_student_count,
             uint32_t employee_count);
    EduGroup(std::string group_name,
             std::string group_leader,
             uint32_t grad_student_count,
             uint32_t employee_count,
             std::vector<Course> courses);

    void print() const override;
    void print(std::ostream& out) const override;

    void dbread(std::istream& in) override;
    void setup_courses(std::string& line, std::vector<std::string>& PS1);
    bool is_selected(uint64_t comp, uint64_t number) override;
    void set_fields(std::string& line, std::vector<std::string>& PS1) override;
    uint64_t cost() const override;
    ~EduGroup() override;
};

const std::string EDU_HELP = "Education group help:"
                             "\n"
                             + GROUP_HELP
                             + "    courses - switch mode for editing courses"
                               "\n"
                               "    help    - get this help screen"
                               "\n";
const std::string EDU_COURSES_HELP = "Course editing mode help:"
                                     "\n"
                                     "    push - add the buffered entry"
                                     "\n"
                                     "    pop  - remove the last added entry"
                                     "\n"
                                     "    cost - set cost per student"
                                     "\n"
                                     "    stc  - set student count"
                                     "\n"
                                     "    end  - exit this mode"
                                     "\n";
#endif