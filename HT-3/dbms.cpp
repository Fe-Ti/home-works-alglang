// Copyright 2021 Fe-Ti <btm.007@mail.ru>

#include "dbms.hpp"

void
DBMS::get_db_name()
{
    std::cout << "Enter the database name: ";
    std::cin >> db_name;
}

void
DBMS::figure_out_db_name(const std::string& question)
{
    std::string q;
    std::cout << question << db_name << "?(y/n)" << std::endl;
    std::cin >> q;
    if(std::cin.fail()) {
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cin.clear();
    }
    if(q == "n" || q == "N") {
        get_db_name();
    } else if(q == "y" || q == "Y") {
        return;
    } else {
        db_name = q; // if not y|Y|n|N then we have a db_name to load
    }
}

bool
DBMS::is_empty_db()
{
    if(group_vec.size() == 0) {
        std::cout << "The database is empty" << std::endl;
        return true;
    }
    return false;
}
void
DBMS::save()
{
    figure_out_db_name("Do you want to save as ");
    std::ofstream f_out(db_name + db_name_suffix);
    if(!f_out.is_open()) {
        std::cout << "Error opening the database!" << std::endl;
    }
    for(Group* i : group_vec) {
        i->print(f_out);
        std::cout << "Saved 1 entry" << std::endl;
    }
    f_out.close();
}

void
DBMS::load()
{
    figure_out_db_name("Do you want to load ");
    std::ifstream f_in(db_name + db_name_suffix);
    if(!f_in.is_open()) {
        std::cout << "Database does not exist" << std::endl;
        return;
    }
    std::string line;
    line = db_name;
    clear(); // clear contents of the database
    db_name = line;
    std::getline(f_in, line);
    while(!f_in.eof()) {
        if(line == "EDU_GROUP") {
            std::cout << "Loading Education Group entry..." << std::endl;
            group_vec.push_back(new EduGroup("new", "new"));
            group_vec[group_vec.size() - 1]->dbread(f_in);
        } else if(line == "SCI_GROUP") {
            std::cout << "Loading Science Group entry..." << std::endl;
            group_vec.push_back(new SciGroup("new", "new"));
            group_vec[group_vec.size() - 1]->dbread(f_in);
        }
        std::cout << "Looked through 1 item." << std::endl;
        std::getline(f_in, line);
    }
    f_in.close();
}

void
DBMS::drop()
{
    list();
    std::string db_to_del;
    std::cout << "What database do you want to drop?\n"
                 "Enter non-existing database name to cancel,\n"
                 "or enter 'all' to drop all databases."
              << std::endl;
    std::cin >> db_to_del;
    db_to_del += db_name_suffix;
    if(db_to_del == "all" + db_name_suffix) {
        stdfs::path buffer;
        for(auto& item : stdfs::directory_iterator(stdfs::current_path())) {
            buffer = stdfs::relative(item.path());
            if(buffer.string().find(db_name_suffix) != std::string::npos)
                stdfs::remove(buffer);
        }
    } else {
        try {
            if(stdfs::remove(stdfs::path(db_to_del)))
                std::cout << "Success!" << std::endl;
            else
                std::cout << "No changes." << std::endl;
        } catch(stdfs::filesystem_error& e) {
            std::cout << "An error occured: " << e.what() << std::endl;
        }
    }
}

void
DBMS::clear() // clearing all fields
{
    for(Group* i : group_vec) {
        delete i;
    }
    group_vec.clear();
    db_name.clear();
}

void
DBMS::list()
{
    std::cout << "Available databases:" << std::endl;
    std::string buffer;
    for(auto& item : stdfs::directory_iterator(stdfs::current_path())) {
        buffer = stdfs::relative(item.path()).string();
        if(buffer.find(db_name_suffix) != std::string::npos)
            std::cout << "    "
                      << buffer.substr(0, buffer.size() - db_name_suffix.size())
                      << std::endl;
    }
}

void
DBMS::add_item()
{
    std::string gtype;
    std::cout << "Choose group type ('sci' or 'edu'):" << std::endl;
    while(gtype != "edu" && gtype != "sci")
        std::cin >> gtype;

    if(gtype == "edu") {
        group_vec.push_back(new EduGroup("New", "New"));
    } else if(gtype == "sci") {
        group_vec.push_back(new SciGroup("New", "New"));
    }
    group_vec[group_vec.size() - 1]->set_fields(action, PS1);
    std::cout << "Added new group." << std::endl;
}

void
DBMS::del_item()
{
    if(is_empty_db()) {
        return;
    }
    uint64_t pk_to_del;
    std::cout << "Enter group ID: ";
    std::cin >> pk_to_del;
    try {
        delete group_vec.at(pk_to_del);
        group_vec.erase(group_vec.begin() + pk_to_del);
        std::cout << "Erased the entry" << std::endl;
    } catch(const std::out_of_range& e) {
        std::cout << e.what() << std::endl;
    }
}

void
DBMS::modify_item()
{
    if(is_empty_db()) {
        return;
    }
    uint64_t pk_to_mod;
    std::cout << "Enter group ID: ";
    std::cin >> pk_to_mod;
    try {
        group_vec.at(pk_to_mod)->set_fields(action, PS1);
    } catch(const std::out_of_range& e) {
        std::cout << e.what() << std::endl;
    }
}

void
DBMS::rename_db()
{
    std::cout << "Enter the databse new name:" << std::endl;
    std::cin >> db_name;
}

void
DBMS::print_items()
{
    if(is_empty_db()) {
        return;
    }
    std::cout << "Enter entry ID(from 0 to " << group_vec.size()
              << ") or 'all' to print all." << std::endl;
    std::cin >> action;
    if(action == "all") {
        for(Group* i : group_vec)
            i->print();
    } else {
        try {
            group_vec.at(std::stoi(action));
        } catch(const std::invalid_argument& e) {
            std::cout << "Invalid argument." << std::endl;
        } catch(const std::out_of_range& e) {
            std::cout << "This ID does not exist." << std::endl;
        }
    }
}

void
DBMS::select_items()
{
    if(is_empty_db()) {
        return;
    }
    uint64_t t;
    std::cout
      << "Enter 0 to select entries with 'graduated student count > number'"
         " or 1 to select entries with 'graduated student count / employee "
         "count >= number'."
      << std::endl;
    std::cin >> t;
    uint64_t n;
    std::cout << "Enter the number." << std::endl;
    std::cin >> n;

    std::vector<Group*> pk_selection;
    std::cout << "Selecting";
    for(Group* i : group_vec) {
        if(i->is_selected(t, n)) {
            std::cout << "!";
            pk_selection.push_back(i);
        } else {
            std::cout << ".";
        }
    }
    std::cout << std::endl;
    std::cout << "Found " << pk_selection.size() << " entries" << std::endl;
    for(Group* i : pk_selection) {
        i->print();
    }
}

void
DBMS::sort_items()
{
    uint8_t o;
    std::cout << "Enter 0 to sort entries by name or 1 by employee count."
              << std::endl;
    std::cin >> o;
    if(o == 0) {
        std::sort(group_vec.begin(), group_vec.end(), [](Group* a, Group* b) {
            return a->group_name > b->group_name;
        });
    } else if(o == 1) {
        std::sort(group_vec.begin(), group_vec.end(), [](Group* a, Group* b) {
            return a->employee_count > b->employee_count;
        });
    } else {
        std::cout << "Unknown sort pattern" << std::endl;
    }
}

void
DBMS::edit()
{
    PS1.push_back("<edit>");
    action = "help";
    while(action != "exit" && action != "E") {
        if(action == "add") {
            add_item();
        } else if(action == "del") {
            del_item();
        } else if(action == "modify"|| action == "mod") {
            modify_item();
        } else if(action == "rename-db" || action == "mv") {
            rename_db();
        } else if(action == "print" || action == "p") {
            print_items();
        } else if(action == "select"||action == "s") {
            select_items();
        } else if(action == "help" || action == "h") {
            std::cout << EDIT_HELP << std::endl;
        } else if(action != "exit" && action != "E") {
            std::cout << action << ": command not found" << std::endl;
        }
        std::cout << PS1_str(PS1) << "Enter a command: ";
        std::cin >> action;
    }
    PS1.pop_back();
}

void
DBMS::get_total_income()
{
    uint64_t sum = 0;
    for(Group* i : group_vec) {
        sum += i->cost();
    }
    std::cout << "Total income is " << sum << std::endl;
}

void
DBMS::find_by_leader_name()
{
    std::vector<Group*> selection;
    std::string name;
    std::cout << "Enter a group leader name: ";
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::getline(std::cin, name);
    for(Group* i : group_vec) {
        if(i->group_leader == name) {
            selection.push_back(i);
        }
    }
    std::cout << "Search result:" << std::endl;
    if(selection.empty()) {
        std::cout << "Nothing was found." << std::endl;
    }
    for(Group* i : selection) {
        i->print();
    }
}

DBMS::~DBMS()
{
    clear();
}

/*void
DBMS::configure()
{
}*/
/*    if(DEBUG == 1)
        std::cout << "Entered into  function." << std::endl;

    if(DEBUG == 1)
        std::cout << "Exited  function." << std::endl;*/
