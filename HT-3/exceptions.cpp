// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#include "exceptions.hpp"
database_exception::database_exception(const std::string& error)
{
    this->error = error;
}
const char*
database_exception::what() const noexcept
{
    return error.c_str();
}
database_exception::~database_exception()
{
}
