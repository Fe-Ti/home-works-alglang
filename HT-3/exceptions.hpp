// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#ifndef DB_EXCEPTIONS
#define DB_EXCEPTIONS

#include <exception>
#include <string>

// Just because I ~can~ need custom exception for S-2/L-04
class database_exception : public std::exception
{
    std::string error;

  public:
    database_exception(const std::string& error);
    const char* what() const noexcept override;
    ~database_exception() override;
};

#endif