// Copyright 2021 Fe-Ti <btm.007@mail.ru>

#include "dbms.hpp"

const std::string HELP
  = "Main help:"
    "\n"
    "    save (S)   - save loaded database"
    "\n"
    "    load (l)   - load database"
    "\n"
    "    create     - create new database the same thing"
    "\n"
    "    (clear|c)     as clear current datasbase"
    "\n"
    "    drop (D)   - drop databases"
    "\n"
    "    list (ls)  - list available databases"
    "\n"
    "    edit (e)   - edit current database"
    "\n"
    "    print (p)  - print all elements or by ID"
    "\n"
    "    select (s) - print elements selected by som criteria"
    "\n"
    "    find (f)   - find group by leader name"
    "\n"
    "    getti (g)  - get total income"
    "\n"
    "    exit       - exit the DMBS"
    "\n";

const std::string SPLASH = "\n"
                           " ###  ###  #   #  /####\n"
                           " #  # #  # ## ## ##/   \n"
                           " #  # ###  # # #  ###            #   ###\n"
                           " #  # #  # #   #   /##   #   /# ##     #\n"
                           " ###  ###  #   #####/     # /#   #    # \n"
                           "                           #/    # # ###\n";

int
main()
{
    DBMS dbms;
    std::cout << SPLASH << std::endl;
    dbms.action = "help";
    while(dbms.action != "exit") {
        if(dbms.action == "save" || dbms.action == "S") {
            dbms.save();
        } else if(dbms.action == "load" || dbms.action == "l") {
            dbms.load();
        } else if(dbms.action == "create" || dbms.action == "clear"
                  || dbms.action == "c") {
            dbms.clear();
        } else if(dbms.action == "drop" || dbms.action == "D") {
            dbms.drop();
        } else if(dbms.action == "list" || dbms.action == "ls") {
            dbms.list();
        } else if(dbms.action == "edit" || dbms.action == "e") {
            dbms.edit();
        } else if(dbms.action == "print" || dbms.action == "p") {
            dbms.print_items();
        } else if(dbms.action == "select" || dbms.action == "s") {
            dbms.select_items();
        } else if(dbms.action == "find" || dbms.action == "f") {
            dbms.find_by_leader_name();
        } else if(dbms.action == "getti" || dbms.action == "g") {
            dbms.get_total_income();
        } else if(dbms.action == "help" || dbms.action == "h") {
            std::cout << HELP << std::endl;
        } else if(dbms.action != "exit") {
            std::cout << dbms.action << ": command not found" << std::endl;
        }
        std::cout << PS1_str(dbms.PS1) << "Enter a command (or 'help'): ";
        std::cin >> dbms.action;
    }

    return 0;
}