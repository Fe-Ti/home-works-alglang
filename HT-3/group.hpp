// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#ifndef GROUP
#define GROUP

#include <algorithm>
#include <fstream>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

#include "exceptions.hpp"

class Group
{
  public:
    // These are public fields for easier access (both derivatives have them)
    // I should implement get_${field_name} and set_${field_name} functions
    std::string group_name;
    std::string group_leader;
    uint32_t grad_student_count;
    uint32_t employee_count;

    virtual void print() const;
    virtual void print(std::ostream& out) const;
    virtual void dbread(std::istream& in);
    virtual bool is_selected(uint64_t comp, uint64_t number);
    virtual void set_fields(std::string& line, std::vector<std::string>& PS1);

    virtual uint64_t cost() const;

    virtual ~Group();
};
const std::string GROUP_HELP = "    END     - stop editing"
                               "\n"
                               "    name    - enter the group name"
                               "\n"
                               "    leader  - enter the leader name"
                               "\n"
                               "    gsc     - enter graduated students count"
                               "\n"
                               "    emc     - enter employee count"
                               "\n";

std::string
PS1_str(std::vector<std::string>& PS1);
/*
template<typename T>
bool
compare(T val0, std::string& comp, T val1);*/

/*
void
get_word(std::string& word, const std::string& prompt);*/

#endif
