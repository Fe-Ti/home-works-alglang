// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#include "education_group.hpp"
void
Course::print()
{
    std::cout << "Course student count: " << student_count << std::endl;
    std::cout << "Course cost per student: " << cost_per_student << std::endl;
}
EduGroup::EduGroup()
{
    this->grad_student_count = 0;
    this->employee_count = 0;
    this->courses = {};
}
EduGroup::EduGroup(std::string group_name, std::string group_leader)
{
    this->group_name = group_name;
    this->group_leader = group_leader;
    this->grad_student_count = 0;
    this->employee_count = 0;
    this->courses = {};
}
EduGroup::EduGroup(std::string group_name,
                   std::string group_leader,
                   uint32_t grad_student_count,
                   uint32_t employee_count)
{
    this->group_name = group_name;
    this->group_leader = group_leader;
    this->grad_student_count = grad_student_count;
    this->employee_count = employee_count;
    this->courses = {};
}
EduGroup::EduGroup(std::string group_name,
                   std::string group_leader,
                   uint32_t grad_student_count,
                   uint32_t employee_count,
                   std::vector<Course> courses)
{
    this->group_name = group_name;
    this->group_leader = group_leader;
    this->grad_student_count = grad_student_count;
    this->employee_count = employee_count;
    this->courses = courses;
}

void
EduGroup::print() const
{
    std::cout << "---- Education group ----" << std::endl;
    std::cout << "| Name:   " << group_name << std::endl;
    std::cout << "| Leader: " << group_leader << std::endl;
    std::cout << "| Graduated student count: " << grad_student_count;
    std::cout << std::endl;
    std::cout << "| Employee count:          " << employee_count << std::endl;
    if(courses.size() != 0) {
        std::cout << "| Courses:" << std::endl;
        for(Course i : courses) {
            std::cout << "|----" << std::endl;
            std::cout << "|    Student count:    " << std::endl
                      << i.student_count << std::endl;
            std::cout << "|    Cost per student: " << std::endl
                      << i.cost_per_student << std::endl;
        }
    }
    std::cout << "-----------END-----------" << std::endl;
}
void
EduGroup::print(std::ostream& out) const
{
    out << "EDU_GROUP" << std::endl;
    out << "group name:" << std::endl << group_name << std::endl;
    out << "group leader:" << std::endl << group_leader << std::endl;
    out << "grad student count:" << std::endl
        << grad_student_count << std::endl;
    out << "employee count:" << std::endl << employee_count << std::endl;
    if(courses.size() != 0) {
        out << "COURSES" << std::endl;
        for(Course i : courses) {
            out << "student count:" << std::endl
                << i.student_count << std::endl;
            out << "cost per student:" << std::endl
                << i.cost_per_student << std::endl;
            out << "end item" << std::endl;
        }
        out << "END" << std::endl;
    }
    out << "END" << std::endl;
}

void
EduGroup::dbread(std::istream& in)
{
    std::string line;
    std::getline(in, line);
    while(line != "END") {
        // std::cout << line << std::endl;
        if(line == "group name:") {
            std::getline(in, group_name);
        } else if(line == "group leader:") {
            std::getline(in, group_leader);
        } else if(line == "grad student count:") {
            std::getline(in, line);
            grad_student_count = std::stoi(line);
        } else if(line == "employee count:") {
            std::getline(in, line);
            employee_count = std::stoi(line);
        } else if(line == "COURSES") {
            Course buffer = { 0, 0 };
            while(line != "END") {
                if(line == "student count:") {
                    std::getline(in, line);
                    buffer.student_count = std::stoi(line);
                } else if(line == "cost per student:") {
                    std::getline(in, line);
                    buffer.cost_per_student = std::stoi(line);
                } else if(line == "end item") {
                    courses.push_back(buffer);
                }
                std::getline(in, line);
            }
        }
        std::getline(in, line);
    }
}
void
EduGroup::setup_courses(std::string& line, std::vector<std::string>& PS1)
{
    PS1.push_back("<courses>");
    // std::string line = "help";
    Course buffer = { 0, 0 };
    while(line != "end") {
        if(line == "push") {
            courses.push_back(buffer);
            print();
            std::cout << PS1_str(PS1) << "Enter an option (or 'help')"
                      << std::endl;
        } else if(line == "pop") {
            courses.pop_back();
            print();
            std::cout << PS1_str(PS1) << "Enter an option (or 'help')"
                      << std::endl;
        } else {
            if(line == "stc") {
                std::cout << "Enter student count: ";
                std::cin >> buffer.student_count;
            } else if(line == "cost") {
                std::cout << "Enter cost per student: ";
                std::cin >> buffer.cost_per_student;
            } else if(line == "help") {
                std::cout << EDU_COURSES_HELP << std::endl;
            } else {
                std::cout
                  << line
                  << ": option not found. Enter 'help' to view all available"
                  << std::endl;
            }
            std::cout << PS1_str(PS1) << "Buffered course:" << std::endl;
            buffer.print();
        }
        std::cin >> line;
    }
    PS1.pop_back();
}

bool
EduGroup::is_selected(uint64_t comp, uint64_t number)
{
    if(comp == 0) {
        return grad_student_count > number;
    } else if(comp == 1 && employee_count != 0) {
        return (grad_student_count / employee_count) >= number;
        // Yep, here we get integer division, because we are counting people
    }
    return false;
}

void
EduGroup::set_fields(std::string& line, std::vector<std::string>& PS1)
{
    PS1.push_back("<fields>");
    // std::string line = "help";
    while(line != "END") {
        if(line == "name") {
            std::cout << "Enter group name: ";
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::getline(std::cin, group_name);
        } else if(line == "leader") {
            std::cout << "Enter group leader: ";
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::getline(std::cin, group_leader);
        } else if(line == "gsc") {
            std::cout << "Enter amount of graduated studens: ";
            std::cin >> grad_student_count;
        } else if(line == "emc") {
            std::cout << "Enter employee count: ";
            std::cin >> employee_count;
        } else if(line == "courses") {
            std::cout << "Enter courses:" << std::endl;
            setup_courses(line, PS1);
        } else {
            std::cout << line << ": option not found. "
                      << "Enter 'help' to view all available" << std::endl;
        }
        print();
        if(line == "help") {
            std::cout << EDU_HELP << std::endl;
        }
        std::cout << PS1_str(PS1) << "Enter an option (or 'help')" << std::endl;
        std::cin >> line;
    }
    PS1.pop_back();
}
uint64_t
EduGroup::cost() const
{
    if(!courses.empty()) {
        uint64_t sum = 0;
        for(Course i : courses) {
            sum += i.cost_per_student * i.student_count;
        }
        return sum;
    }
    return 0;
}
EduGroup::~EduGroup()
{
}
