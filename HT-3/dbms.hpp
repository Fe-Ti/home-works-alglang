// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#ifndef DBMS_CLASS
#define DBMS_CLASS

#include <filesystem>
#include <functional>
#include <sstream>
// #include <cstdio> // to std::remove single file

namespace stdfs = std::filesystem;

#include "education_group.hpp"
#include "science_group.hpp"

class DBMS
{
    std::string db_name; // 'selected' database
    std::vector<Group*> group_vec;
    // path ?
    bool is_empty_db();
    void get_db_name();
    void figure_out_db_name(const std::string& question);

  public:
    std::vector<std::string> PS1 = { "<main>" };
    std::string action;
    void save();
    void load();
    void create();
    void drop();
    void clear();
    void list();

    void add_item();
    void del_item();
    void modify_item();
    void rename_db();
    void print_items();
    void select_items();
    void sort_items();
    void edit();

    void get_total_income();
    void find_by_leader_name();

    void configure();
    ~DBMS();
};

const std::string EDIT_HELP = "Edit mode help:"
                              "\n"
                              "    add            - add a new entry"
                              "\n"
                              "    del            - delete an entry"
                              "\n"
                              "    modify (mod)   - modify an entry"
                              "\n"
                              "    rename-db (mv) - rename the database"
                              "\n"
                              "    print (p)      - print all elements or by ID"
                              "\n"
                              "    select (s)     - select entries by some criteria"
                              "\n"
                              "    exit (E)       - exit the mode"
                              "\n";
const std::string db_name_suffix = ".u_cant_touch_this";
// const uint8_t DEBUG = 1;
#endif
