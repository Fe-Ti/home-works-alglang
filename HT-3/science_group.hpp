// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#ifndef SCI_GROUP
#define SCI_GROUP

#include "group.hpp"

struct Project {
    std::string project_name;
    uint64_t cost;
    void print();
};

class SciGroup : public Group
{
  public:
    std::vector<Project> projects;

    SciGroup();
    SciGroup(std::string group_name, std::string group_leader);
    SciGroup(std::string group_name,
             std::string group_leader,
             uint32_t grad_student_count,
             uint32_t employee_count);
    SciGroup(std::string group_name,
             std::string group_leader,
             uint32_t grad_student_count,
             uint32_t employee_count,
             std::vector<Project> projects);

    void print() const override;
    void print(std::ostream& out) const override;

    void dbread(std::istream& in) override;
    void setup_projects(std::string& line, std::vector<std::string>& PS1);
    bool is_selected(uint64_t comp, uint64_t number) override;
    void set_fields(std::string& line, std::vector<std::string>& PS1) override;
    uint64_t cost() const override;

    ~SciGroup() override;
};

const std::string SCI_HELP = "Science group help:"
                             "\n"
                             + GROUP_HELP
                             + "    projects - switch mode for editing projects"
                               "\n"
                               "    help    - get this help screen"
                               "\n";
const std::string SCI_PROJECTS_HELP = "Project editing mode help:"
                                      "\n"
                                      "    push - add the buffered entry"
                                      "\n"
                                      "    pop  - remove the last added entry"
                                      "\n"
                                      "    name - set project name"
                                      "\n"
                                      "    cost - set project cost"
                                      "\n"
                                      "    end  - exit this mode"
                                      "\n";

#endif