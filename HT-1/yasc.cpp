// Copyright 2020 Fe-Ti <btm.007@mail.ru>

//#include <bitset>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

const std::string HELP =
  "YASC program v2.0\n"
  "\n"
  "-m, --mode <mode>     - set mode to:\n"
  "                         e - encrypting (default)\n"
  "                         d - decrypting\n"
  "\n"
  "-k, --key             - set key (a decimal number)\n"
  "\n"
  "-s, --shift           - set shift (a decimal number)\n"
  "                        if shift>=0 then >>, else <<\n"
  "\n"
  "-kf, --key-file       - specify key file name\n"
  "\n"
  "-if, --input-file     - specify input file name\n"
  "\n"
  "-of, --output-file    - specify output file name\n"
  "\n"
  "-h, --help            - show this help message\n"
  "\n"
  "Usage:\n"
  "yasc [OPTIONS] -if <input file> -of <output file> \n"
  "";

const uint32_t blocksize = 8; // this is not nesessary, but I did
const uint32_t bn_size = 8;   // block node size (used in cyclic shift function)

struct Block { // the main character of YASC :-)
    uint8_t data[blocksize];
    Block()
    {
        for(uint32_t i = 0; i < blocksize; ++i) {
            data[i] = '\0';
        }
    }
};

/* -------------------------------------------------------------
 * Boolean flags
 *
 * There are some functions which declare command line arguments
 * used in the YASC.
 * Args format is close to Unix command line utilities,
 * e.g. there are equivalent keys like -m and --mode.
 * -------------------------------------------------------------
 */

bool
is_if_flag(char* arg)
{
    return (arg == std::string{ "-if" }) ||
           (arg == std::string{ "--input-file" });
}
bool
is_of_flag(char* arg)
{
    return (arg == std::string{ "-of" }) ||
           (arg == std::string{ "--output-file" });
}
bool
is_key_flag(char* arg)
{
    return (arg == std::string{ "-k" }) || (arg == std::string{ "--key" });
}
bool
is_kf_flag(char* arg)
{
    return (arg == std::string{ "-kf" }) ||
           (arg == std::string{ "--key-file" });
}
bool
is_mode_flag(char* arg)
{
    return (arg == std::string{ "-m" }) || (arg == std::string{ "--mode" });
}

bool
is_shift_flag(char* arg)
{
    return (arg == std::string{ "-s" }) || (arg == std::string{ "--shift" });
}

/* -------------------------------------
 * The end of flags section
 * -------------------------------------
 * The begin of actual functions section
 * -------------------------------------
 */

/* --------------------------
 *   FFFFFF  IIIIII  oooooo
 *   FF        II    oo  oo
 *   FFFF      II    oo  oo
 *   FF        II    oo  oo
 *   FF      IIIIII  oooooo
 * --------------------------
 */
void
clear_buffer(Block& buffer)
{
    for(uint8_t i = 0; i < blocksize; ++i)
        buffer.data[i] = '\0';
}

int
read_file(std::string input_file_name,
          std::vector<Block>& b_vect) // reading files directly into blocks
{
    std::ifstream input_file(input_file_name,
                             std::ios::binary); // opening file in binary mode

    if(!(input_file.is_open())) // checking if file opened
        return 1;

    Block buffer_b;
    while(!input_file.eof()) {
        input_file.read(reinterpret_cast<char*>(&buffer_b), sizeof(buffer_b));
        b_vect.push_back(buffer_b);
        clear_buffer(buffer_b);
    }
    input_file.close(); // closing file stream

    return 0;
}

/*
void
printblk(Block& blk)
{
    uint8_t* data = blk.data;
    for(uint32_t i = 0; i < blocksize; ++i)
        std::cout << std::bitset<8>(data[i]) << " ";
    std::cout << std::endl;
}
*/

int
write_file(std::vector<Block>& b_vect,
           std::string& output_file_name,
           int16_t& mode_f) // writing blocks directly into files

{
    std::ofstream output_file(output_file_name, std::ios::binary);
    if(!(output_file.is_open())) // the same check as in reading function
        return 2;

    for(size_t i = 0; i < b_vect.size() - 2 * mode_f; ++i) {
        output_file.write(reinterpret_cast<char*>(&b_vect[i]),
                          sizeof(b_vect[i]));
    }

    if(mode_f == 1) {
        uint64_t last_index = b_vect.size() - 2;
        // printblk(b_vect[last_index]);
        for(uint32_t i = 0; i < blocksize; ++i) {
            if(b_vect[last_index].data[i] != '\0') {
                output_file.write(
                  reinterpret_cast<char*>(&b_vect[last_index].data[i]),
                  sizeof(b_vect[last_index].data[i]));
            }
        }
    }

    output_file.close(); // closing file stream
    return 0;
}
/* ----------------------------------------------------
 *   EEEEEE  NN   N  DDDDD     FFFFFF  IIIIII  OOOOOO
 *   EE      NNN  N  DD   D    FF        II    OO  OO
 *   EEEEE   NN N N  DD   D    FFFF      II    OO  OO
 *   EE      NN  NN  DD   D    FF        II    OO  OO
 *   EEEEEE  NN   N  DDDDD     FF      IIIIII  OOOOOO
 * ----------------------------------------------------
 */

/* ----------------------------------------------
 *   CCCCCC  SSSSSS  HH      II    FFFF    TT
 *   CC      SS      HH            FF    TTTTTT
 *   CC        SS    HHHHHH  II  FFFFFF    TT
 *   CC          SS  HH  HH  II    FF      TT
 *   CCCCCC  SSSSSS  HH  HH  II    FF      TTTT
 * ----------------------------------------------
 */
void
operator<<(Block& blk, int16_t& shift)
{
    uint8_t* dcopy1;
    dcopy1 = new uint8_t[blocksize];
    for(uint32_t i = 0; i < blocksize; ++i)
        dcopy1[i] = blk.data[i];

    for(uint32_t i = 0; i < blocksize; ++i) {
        blk.data[i] = (dcopy1[i] << shift) |
                      (dcopy1[(i + 1) % blocksize] >> (bn_size - shift));
    }
    delete[] dcopy1; // freeing memory
}

void
operator>>(Block& blk, int16_t& shift)
{
    uint8_t* dcopy1;
    dcopy1 = new uint8_t[blocksize];
    for(uint32_t i = 0; i < blocksize; ++i)
        dcopy1[i] = blk.data[i];

    for(uint32_t i = 0; i < blocksize; ++i) {
        blk.data[i] =
          (dcopy1[i] >> shift) |
          (dcopy1[(i + blocksize - 1) % blocksize] << (bn_size - shift));
    }

    delete[] dcopy1; // freeing memory
}

void
byte_data_cshift(Block& blk, int16_t& shift, int8_t& direction)

// this shift is intended for situations
// when the shift size is greater than 7 bits
{
    uint8_t* dcopy1;
    dcopy1 = new uint8_t[blocksize];
    for(uint32_t i = 0; i < blocksize; ++i)
        dcopy1[i] = blk.data[i];
    if(direction < 0) {
        for(uint32_t i = 0; i < blocksize; ++i) // just swapping the right bytes
            blk.data[(i + shift) % blocksize] = dcopy1[i];
    } else {
        for(uint32_t i = 0; i < blocksize; ++i)
            blk.data[(i + (blocksize - shift % blocksize)) % blocksize] =
              dcopy1[i];
    }

    delete[] dcopy1;
}

void
cyclic_shift(std::vector<Block>& b_vect,
             int16_t& shift) // all checks for shifting
                             // are done there
                             // and proper functions are called
{
    std::cout << shift << " " << b_vect.size() << std::endl;

    int8_t direction = 1;
    if(shift < 0) {
        shift = -shift;
        direction = -1;
    }

    std::cout << shift << " " << b_vect.size() << std::endl;

    int16_t byte_shift = shift / bn_size; // workaround for Grand Shift Activity
    int16_t bit_shift = shift % bn_size;

    for(size_t i = 0; i < b_vect.size(); ++i) {
        if(byte_shift > 0) { // checking if we should move bytes
            byte_data_cshift(b_vect[i], byte_shift, direction);
        }
        if(bit_shift > 0) { // checking if we should move bits
            if(direction > 0) {
                b_vect[i] >> bit_shift;
            } else {
                b_vect[i] << bit_shift;
            }
        }
    }
}

/* -------------------------------------------------------------------------
 *   EEEEEE  NN   N  DDDDD      CCCCCC  SSSSSS  HH      II    FFFF    TT
 *   EE      NNN  N  DD   D     CC      SS      HH            FF    TTTTTT
 *   EEEE    NN N N  DD   D     CC        SS    HHHHHH  II  FFFFFF    TT
 *   EE      NN  NN  DD   D     CC          SS  HH  HH  II    FF      TT
 *   EEEEEE  NN   N  DDDDD      CCCCCC  SSSSSS  HH  HH  II    FF      TTTT
 * -------------------------------------------------------------------------
 */

/* ----------------------------------------------------------
 *   GGGGGG    AA    M    M  M    M  IIIIII  NN   N  GGGGGG
 *   GG      AA  AA  MM  MM  MM  MM    II    NNN  N  GG
 *   GG  GG  AA  AA  M MM M  M MM M    II    NN N N  GG  GG
 *   GG  GG  AAAAAA  M    M  M    M    II    NN  NN  GG  GG
 *   GGGGGG  AA  AA  M    M  M    M  IIIIII  NN   N  GGGGGG
 * ----------------------------------------------------------
 */

Block&
irradiate(Block& blk, uint64_t& digikey)
{
    // overload ^ for blocks
    std::srand(digikey);
    digikey = std::rand();
    for(uint8_t i = 0; i < sizeof(blk); ++i) {
        blk.data[i] = blk.data[i] ^ std::rand(); //
    }
    return blk;
}

void
gamming(std::vector<Block>& b_vec1, uint64_t& digikey)
{
    // overload ^ for vectors with blocks
    for(size_t i = 0; i < b_vec1.size(); ++i) {
        b_vec1[i] = irradiate(b_vec1[i], digikey);
    }
}

/* ------------------------------------------------------------------
 *       //  GGGGGG    AA    M    M  M    M  IIIIII  NN   N  GGGGGG
 *      //   GG      AA  AA  MM  MM  MM  MM    II    NNN  N  GG
 *     //    GG  GG  AA  AA  M MM M  M MM M    II    NN N N  GG  GG
 *    //     GG  GG  AAAAAA  M    M  M    M    II    NN  NN  GG  GG
 *   //      GGGGGG  AA  AA  M    M  M    M  IIIIII  NN   N  GGGGGG
 * ------------------------------------------------------------------
 */
void
encrypting(std::string& input_file_name,
           std::string& output_file_name,
           std::string& key,
           int16_t shift)
{
    uint64_t digikey = std::stoi(key); // replace string with 64 bit integer
    int16_t mode_f = 0;
    std::vector<Block> b_vect;

    read_file(input_file_name, b_vect); // reading

    srand(digikey);
    gamming(b_vect, digikey);

    cyclic_shift(b_vect, shift);                  // shifting
    write_file(b_vect, output_file_name, mode_f); // writing
}

void
decrypting(std::string& input_file_name,
           std::string& output_file_name,
           std::string& key,
           int16_t shift)
{
    uint64_t digikey = std::stoi(key); // replace string with 64 bit integer
    int16_t mode_f = 1;
    std::vector<Block> b_vect;

    read_file(input_file_name, b_vect); // reading

    cyclic_shift(b_vect, shift); // shifting
    srand(digikey);
    gamming(b_vect, digikey);

    write_file(b_vect, output_file_name, mode_f); // writing
}

int
main(int argc, char* argv[])
{
    std::string input_file_name = "";
    std::string output_file_name = "";
    char mode = 'e';
    std::string key = "";
    int16_t shift = 0;

    // reading command line parameters
    for(int i = 1; i < argc; ++i) {
        if(is_if_flag(argv[i])) {
            input_file_name = argv[i + 1];
            ++i;
        } else {
            if(is_mode_flag(argv[i])) {
                mode = *argv[i + 1];
                ++i;
            } else {
                if(is_of_flag(argv[i])) {
                    output_file_name = argv[i + 1];
                    ++i;
                } else {
                    if(is_key_flag(argv[i])) {
                        key = argv[i + 1];
                        ++i;
                    } else {
                        if(is_kf_flag(argv[i])) {
                            std::ifstream kf(argv[i + 1]);
                            kf >> key;
                            kf.close();
                            ++i;
                        } else {
                            if(is_shift_flag(argv[i])) {
                                shift = std::stoi(argv[i + 1]);
                                ++i;
                            }
                        }
                    }
                }
            }
        }
    }
    // checking if we've got all required parameters
    if(input_file_name.empty() || output_file_name.empty()) {
        std::cout << HELP << std::endl;
        return 0;
    }
    if(key.empty()) {
        std::cout << "You must specify a key." << std::endl;
    }

    // selecting mode (with simple protection)
    if(mode == 'd')
        decrypting(input_file_name, output_file_name, key, -shift);
    else if(mode == 'e')
        encrypting(input_file_name, output_file_name, key, shift);
    else {
        std::cout << "Can't understand the mode" << std::endl;
    }
    return 0;
}
