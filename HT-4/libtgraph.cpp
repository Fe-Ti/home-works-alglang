// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#include "libtgraph.hpp"

Screen::Screen()
{
}

Screen::Screen(uint64_t res_x, uint64_t res_y)
{
    buffer.resize(res_x);
    for(std::vector<char>& i : buffer) {
        i.resize(res_y);
        for(char& j : i) {
            j = ' ';
        }
    }
}

void
Screen::resize(uint64_t res_x, uint64_t res_y)
{
    buffer.resize(res_x);
    for(std::vector<char>& i : buffer) {
        i.resize(res_y);
        for(char& j : i) {
            j = ' ';
        }
    }
}

void
Screen::clear()
{
    for(std::vector<char>& i : buffer) {
        for(char& j : i) {
            j = ' ';
        }
    }
}

char&
Screen::raw_buffer(uint64_t pos_x, uint64_t pos_y)
{
    return buffer[pos_x][pos_y];
}

uint64_t
Screen::xsize() const
{
    return buffer.size();
}

uint64_t
Screen::ysize() const
{
    if(buffer.size() > 0)
        return buffer[0].size();
    else
        return 0;
}

void
Screen::print(std::ostream& out) // = std::cout)
{
    for(uint64_t y = 0; y < ysize(); ++y) {
        for(uint64_t x = 0; x < xsize(); ++x) {
            out << buffer[x][y];
        }
        out << std::endl;
    }
}

void
Screen::place_text(uint64_t pos_x,
                   uint64_t pos_y,
                   const std::string& text,
                   bool horizontal) // = true)
{
    uint64_t counter = 0;
    if(pos_x < xsize() && pos_y < ysize()) {
        for(char i : text) {
            if(horizontal && pos_x + counter < xsize())
                buffer[pos_x + counter][pos_y] = i;
            else if(pos_y + counter < ysize())
                buffer[pos_x][pos_y + counter] = i;
            ++counter;
        }
    }
}

void
Screen::rel_place_text(float rel_x,
                       float rel_y,
                       const std::string& text,
                       bool horizontal) // = true)
{
    uint64_t pos_x = xsize() * rel_x;
    uint64_t pos_y = ysize() * rel_y;
    place_text(pos_x, pos_y, text, horizontal);
}
void
Screen::rel_place_text(double rel_x,
                       double rel_y,
                       const std::string& text,
                       bool horizontal) // = true)
{
    uint64_t pos_x = xsize() * rel_x;
    uint64_t pos_y = ysize() * rel_y;
    place_text(pos_x, pos_y, text, horizontal);
}

void
Screen::place_matrix(uint64_t pos_x,
                     uint64_t pos_y,
                     const std::vector<std::vector<char>>& matrix)
{
    if(pos_x < xsize() && pos_y < ysize()) {
        for(uint64_t x = 0; x < matrix.size(); ++x) {
            for(uint64_t y = 0; y < matrix[0].size(); ++y) {
                if(pos_x + x < xsize() && pos_y + y < ysize())
                    buffer[pos_x + x][pos_y + y] = matrix[x][y];
            }
        }
    }
} /*
 void
 Screen::place_matrix(uint64_t pos_x,
                      uint64_t pos_y,
                      const std::vector<std::vector<bool>>& matrix)
 {
     if(pos_x < xsize() && pos_y < ysize()) {
         for(uint64_t x = 0; x < matrix.size(); ++x) {
             for(uint64_t y = 0; y < matrix[0].size(); ++y) {
                 if(pos_x + x < xsize() && pos_y + y < ysize())
                     buffer[pos_x + x][pos_y + y] = matrix[x][y] + 48;
             }
         }
     }
 }*/

void
Screen::rel_place_matrix(float rel_x,
                         float rel_y,
                         const std::vector<std::vector<char>>& matrix)
{
    uint64_t pos_x = xsize() * rel_x;
    uint64_t pos_y = ysize() * rel_y;
    place_matrix(pos_x, pos_y, matrix);
}
void
Screen::rel_place_matrix(double rel_x,
                         double rel_y,
                         const std::vector<std::vector<char>>& matrix)
{
    uint64_t pos_x = xsize() * rel_x;
    uint64_t pos_y = ysize() * rel_y;
    place_matrix(pos_x, pos_y, matrix);
} /*
 void
 Screen::place_inf_line(int64_t a, int64_t b, char ch)
 {

 }
 void
 Screen::place_line(uint64_t x0, uint64_t y0, uint64_t x1, uint64_t y1, char ch)
 {

 }
 void
 Screen::place_circle(uint64_t x0, uint64_t y0, uint64_t r, char ch)
 {
     for(uint64_t x = x0 - r; x < x0 + r; ++x) {
         for(uint64_t y = y0 - r; y < y0 + r; ++y) {
             if(x < xsize() && y < ysize()) {
                 buffer[x][y] = ch;
             }
         }
     }
 }*/