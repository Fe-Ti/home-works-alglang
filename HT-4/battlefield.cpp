// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#include "battlefield.hpp"

BattleField::BattleField()
{
    matrix.resize(DEFAULT_CELL_COUNT);
    for(uint64_t i = 0; i < matrix.size(); ++i) {
        matrix[i].resize(DEFAULT_CELL_COUNT);
        for(uint64_t j = 0; j < matrix[i].size(); ++j) {
            matrix[i][j] = true;
        }
    }
    for(uint64_t i = 4; i > 0; --i) {
        for(uint64_t j = 5 - i; j > 0; --j) {
            ships.push_back(Ship(i, 0, 0, 'h'));
        }
    }
}

bool
BattleField::is_out_of_matrix(Ship rotated_ship)
{
    uint64_t end_position = rotated_ship.cells.size() - 1
                            + (rotated_ship.dir == 'h') * (rotated_ship.x)
                            + (rotated_ship.dir == 'v') * (rotated_ship.y);
    if(end_position > matrix.size() - 1) {
        return true;
    }
    return false; // else
}

bool
BattleField::place_ship(uint64_t sid, uint64_t x, uint64_t y, char dir)
{
    if(sid < ships.size()) {
        Ship backup = ships[sid];
        ships[sid].x = x;
        ships[sid].y = y;
        ships[sid].dir = dir;
        if(is_out_of_matrix(ships[sid])) {
            ships[sid] = backup;
        } else {
            return true;
        }
    }
    return false;
}
bool
BattleField::place_ship(uint64_t sid, uint64_t x, uint64_t y)
{
    return place_ship(sid, x, y, ships[sid].dir);
}

bool
BattleField::rotate_ship(uint64_t sid)
{

    if(sid < ships.size()) {
        ships[sid].rotate();
        if(is_out_of_matrix(ships[sid])) {
            ships[sid].rotate();
            return false;
        }
    }
    return true;
}

uint8_t
BattleField::punch_cell(uint64_t x, uint64_t y) // 0 - error
                                                // 1 - punched a cell
                                                // 2 - punched a ship
                                                // 3 + sid - killed a ship
{
    if(x < matrix.size() && y < matrix.size()) {
        if(matrix[x][y]) {
            matrix[x][y] = false;

            uint8_t sid = 0; // ship id
            for(Ship& s : ships) {
                if(!s.is_killed()) {
                    for(uint64_t i = 0; i < s.cells.size(); ++i) {
                        if(((s.x + (s.dir == 'h') * i) == x)
                           && ((s.y + (s.dir == 'v') * i) == y)) {
                            s.punch_me(i);
                            if(s.is_killed()) {
                                return sid + 3;
                            } else {
                                return 2;
                            }
                        }
                    }
                }
                ++sid;
            }
            return 1;
        }
    }
    return 0;
}