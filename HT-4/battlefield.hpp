// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#ifndef BATTLEFIELD
#define BATTLEFIELD

#include "ship.hpp"

const uint64_t DEFAULT_CELL_COUNT = 10;
const uint64_t MAX_SHIP_SIZE = 4;

class BattleField
{
  public:
    std::vector<std::vector<bool>> matrix;
    std::vector<Ship> ships;

    BattleField();
    bool place_ship(uint64_t sid, uint64_t x, uint64_t y, char dir);
    bool place_ship(uint64_t sid, uint64_t x, uint64_t y);
    bool rotate_ship(uint64_t sid);
    uint8_t punch_cell(uint64_t x, uint64_t y);
    bool is_out_of_matrix(Ship rotated_ship);
};
#endif