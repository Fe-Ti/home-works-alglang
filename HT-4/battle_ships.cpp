// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#include "battlefield.hpp"
#include "libtgraph.hpp"

#include <chrono>
#include <limits>
#include <thread>

struct Player {
    std::string name;
    bool is_human;
    // std::string address
};

struct Input {
    char optype;
    uint64_t sid;
    uint64_t x;
    uint64_t y;
    char dir;
};

const uint64_t DEFAULT_PLAYER_COUNT = 2;
const uint64_t DEFAULT_X_RESOLUTION = 80;
const uint64_t DEFAULT_Y_RESOLUTION = 23; // 24th line is used by user input
                                          // (usually) const uint64_t DEFAULT_;
const std::string MAIN_HELP = "Enter:\n"
                              "  play - to play;\n"
                              "  exit - to quit;\n"
                              "  conf - to configure the game;\n"
                              "  help - to view gameplay help.\n";

const std::string GAME_HELP =
  "##--Gameplay--##\n"
  "  <X> is a horizontal coordinate.\n"
  "  <Y> is a vertical coordinate.\n"
  "  Ship IDs are placed at the right corner of the screen.\n"
  "  \n"
  "  Phase 'placing' commands:\n"
  "    0a                  - atomatically place ships (can take a while);\n"
  "    <ship ID> p <X> <Y> - place a ship to coordinates;\n"
  "    <ship ID> r         - rotate a ship;\n"
  "    0!                  - end your turn.\n"
  "  \n"
  "  Phase 'shooting' commands:\n"
  "    <X> <Y> - choose coordinate to be bombed\n"
  "  \n";

class App
{
  public:
    Screen gs;
    std::vector<Player> players;
    std::vector<BattleField> playerbtfs;
    uint64_t curr_player;
    uint64_t p_count;
    std::string info_message;
    std::string phase;
    float difficulty;
    char ALIVE_H_SHIP = 'D';
    char ALIVE_V_SHIP = 'U';
    char DEAD_H_SHIP = 'F';
    char DEAD_V_SHIP = 'F';
    char CLEAN_WATER = '.';
    char BOMBED_WATER = 'X';

    App()
    {
        gs.resize(DEFAULT_X_RESOLUTION, DEFAULT_Y_RESOLUTION);
        players.push_back(Player{ "John Canner", true });
        players.push_back(Player{ "SkiNet", false });
        playerbtfs.resize(DEFAULT_PLAYER_COUNT);
        curr_player = 0;
        p_count = DEFAULT_PLAYER_COUNT;
        difficulty = 33;
        phase = "menu";
    }

    void fill_player_matrix(std::vector<std::vector<char>>& field_matrix)
    {
        field_matrix.resize(playerbtfs[curr_player].matrix.size());

        for(uint64_t x = 0; x < playerbtfs[curr_player].matrix.size(); ++x) {
            field_matrix[x].resize(playerbtfs[curr_player].matrix[x].size());
            for(uint64_t y = 0; y < playerbtfs[curr_player].matrix[x].size();
                ++y) {
                if(playerbtfs[curr_player].matrix[x][y]) {
                    field_matrix[x][y] = CLEAN_WATER;
                } else {
                    field_matrix[x][y] = BOMBED_WATER;
                }
            }
        }
        for(Ship& i : playerbtfs[curr_player].ships) {
            for(uint64_t j = 0; j < i.cells.size(); ++j) {
                if(i.cells[j]) {
                    if(i.dir == 'h')
                        field_matrix[i.x + j][i.y] = ALIVE_H_SHIP;
                    else
                        field_matrix[i.x][i.y + j] = ALIVE_V_SHIP;
                } else {
                    if(i.dir == 'h')
                        field_matrix[i.x + j][i.y] = DEAD_H_SHIP;
                    else
                        field_matrix[i.x][i.y + j] = DEAD_V_SHIP;
                }
            }
        }
    }

    void fill_enemy_matrix(std::vector<std::vector<char>>& field_matrix)
    {
        field_matrix.resize(playerbtfs[curr_enemy()].matrix.size());

        for(uint64_t x = 0; x < playerbtfs[curr_enemy()].matrix.size(); ++x) {
            field_matrix[x].resize(playerbtfs[curr_enemy()].matrix[x].size());
            for(uint64_t y = 0; y < playerbtfs[curr_enemy()].matrix[x].size();
                ++y) {
                if(playerbtfs[curr_enemy()].matrix[x][y]) {
                    field_matrix[x][y] = CLEAN_WATER;
                } else {
                    field_matrix[x][y] = BOMBED_WATER;
                }
            }
        }
        for(Ship& i : playerbtfs[curr_enemy()].ships) {
            for(uint64_t j = 0; j < i.cells.size(); ++j) {
                if(!i.cells[j]) {
                    if(i.dir == 'h')
                        field_matrix[i.x + j][i.y] = DEAD_H_SHIP;
                    else
                        field_matrix[i.x][i.y + j] = DEAD_V_SHIP;
                }
            }
        }
    }

    void setup_n_place_matrixes(std::string& text_object) // used by compose fnc
    {
        std::vector<std::vector<char>> field_matrix;

        text_object = players[curr_player].name + "'s";
        gs.place_text(2, 2, text_object);
        text_object.clear();
        text_object = "field:";
        gs.place_text(4, 3, text_object);
        text_object.clear();
        fill_player_matrix(field_matrix);
        gs.place_matrix(4, 5, field_matrix);
        // gs.print();
        field_matrix.clear();

        text_object = players[curr_enemy()].name + "'s";
        gs.place_text(24, 2, text_object);
        text_object.clear();
        text_object = "field:";
        gs.place_text(26, 3, text_object);
        text_object.clear();
        fill_enemy_matrix(field_matrix);
        gs.place_matrix(26, 5, field_matrix);
        // gs.print();
        for(int8_t i = 48; i < 10 + 48; ++i)
            // playerbtfs[0].matrix.size(); ++i)
            text_object.push_back(i);
        gs.place_text(26, 4, text_object);
        gs.place_text(26, 15, text_object);
        gs.place_text(4, 4, text_object);
        gs.place_text(4, 15, text_object);
        gs.place_text(24, 5, text_object, false);
        gs.place_text(37, 5, text_object, false);
        gs.place_text(2, 5, text_object, false);
        gs.place_text(15, 5, text_object, false);
        text_object.clear();
    }

    void setup_n_place_info(std::string& text_object)
    {
        text_object = players[curr_player].name + "'s ships:";
        gs.rel_place_text(0.55, 0.1, text_object);
        text_object.clear();
        uint16_t counter = 0;
        char status;
        for(Ship& s : playerbtfs[curr_player].ships) {
            if(!s.is_killed())
                status = '#';
            else
                status = '-';
            text_object = status + std::to_string(counter) +
                          " - Size: " + std::to_string(s.cells.size()) +
                          "; Pos:" + std::to_string(s.x) + "," +
                          std::to_string(s.y) + "; Rot: " + s.dir + ";";
            gs.rel_place_text(0.55, 0.2 + counter * 0.05, text_object);
            text_object.clear();
            ++counter;
        }
        gs.place_text(3, gs.ysize() - 3, "Phase: ");
        gs.place_text(7, gs.ysize() - 2, phase);
        gs.place_text(gs.xsize() / 2 + 2, gs.ysize() - 3, "Operation status:");
        gs.place_text(gs.xsize() / 2 + 3, gs.ysize() - 2, info_message);
    }

    void compose_game_screen()
    {
        std::string text_object = "The Terminaltor v0.1.0-pre";
        gs.place_text(0, 0, text_object);
        text_object.clear();
        setup_n_place_matrixes(text_object);
        // horizontal separator;
        for(uint64_t i = 0; i < gs.xsize(); ++i)
            text_object.push_back('-');
        gs.place_text(0, gs.ysize() - 1, text_object);
        text_object.clear();
        // vertical separator;
        for(uint64_t i = 0; i < gs.ysize(); ++i)
            text_object.push_back('|');
        gs.rel_place_text(0.5, 0., text_object, false);
        text_object.clear();
        // uint64_t;
        setup_n_place_info(text_object);
        // gs.print();
    }
    void print_frame()
    {
        if(phase == "placing" || phase == "shooting") {
            compose_game_screen();
            gs.print();
        }
    }
    void configure()
    {
        std::string action;
        std::cout << "Enter a variable name and its value." << std::endl
                  << "Available var-s:" << std::endl
                  << "ALIVE_H_SHIP is " << ALIVE_H_SHIP << std::endl
                  << "ALIVE_V_SHIP is " << ALIVE_V_SHIP << std::endl
                  << "DEAD_H_SHIP is " << DEAD_H_SHIP << std::endl
                  << "DEAD_V_SHIP is " << DEAD_V_SHIP << std::endl
                  << "CLEAN_WATER is " << CLEAN_WATER << std::endl
                  << "BOMBED_WATER is " << BOMBED_WATER << std::endl
                  << "difficulty is " << difficulty << std::endl;
        std::cin >> action;
        if(action == "ALIVE_H_SHIP" || action == "ahs")
            std::cin >> ALIVE_H_SHIP;
        else if(action == "ALIVE_V_SHIP" || action == "avs")
            std::cin >> ALIVE_V_SHIP;
        else if(action == "DEAD_H_SHIP" || action == "dhs")
            std::cin >> DEAD_H_SHIP;
        else if(action == "DEAD_V_SHIP" || action == "dvs")
            std::cin >> DEAD_V_SHIP;
        else if(action == "CLEAN_WATER" || action == "cw")
            std::cin >> CLEAN_WATER;
        else if(action == "BOMBED_WATER" || action == "bw")
            std::cin >> BOMBED_WATER;
        else if(action == "difficulty" || action == "df")
            std::cin >> difficulty;
        else
            std::cout << "Unknown variable." << std::endl;
        if(std::cin.fail()) {
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cin.clear();
        }
    }

    bool is_defeated(uint64_t player_n)
    {
        for(Ship& i : playerbtfs[player_n].ships) {
            if(!i.is_killed())
                return false;
        }
        return true;
    }

    void regenerate_screen()
    {
        gs.clear();
        info_message.clear();
        if(phase == "placing" || phase == "shooting")
            compose_game_screen();
    }

    bool is_game_end() // if support for more than 2 players appears then
                       // refactoring is required
    {
        for(uint64_t i = 0; i < players.size(); ++i) {
            if(is_defeated(i))
                return true;
        }
        return false;
    }

    bool is_correct_positioning(uint64_t player_n, bool silent = false)
    {
        BattleField tmp;
        for(Ship i : playerbtfs[player_n].ships) {
            if(playerbtfs[player_n].is_out_of_matrix(i)) {
                if(!silent)
                    std::cout << "A ship is out of the matrix!" << std::endl;
                return false;
            }
            uint64_t xend =
              i.x + 1 + (i.cells.size()) * (i.dir == 'h') + (i.dir == 'v');
            uint64_t yend =
              i.y + 1 + (i.cells.size()) * (i.dir == 'v') + (i.dir == 'h');
            for(uint64_t x = i.x - (i.x != 0); x < xend; ++x) {
                for(uint64_t y = i.y - (i.y != 0); y < yend; ++y) {
                    if((x < tmp.matrix.size()) && (y < tmp.matrix.size())) {
                        // gs.place_matrix(4, 5, tmp.matrix);
                        // gs.print();
                        if(!tmp.matrix[x][y]) {
                            if(!silent)
                                std::cout << "Overlapping ships! " << x << " "
                                          << y << std::endl;
                            return false;
                        }
                    }
                }
            }
            for(uint64_t j = 0; j < i.cells.size(); ++j)
                tmp.matrix[i.x + j * (i.dir == 'h')][i.y + j * (i.dir == 'v')] =
                  false;
        }
        if(!silent)
            std::cout << "Everything is correct!" << std::endl;
        return true;
    }

    void placing_prompt()
    {
        gs.print();
        std::cout << "INPUT>> ";
    }

    Input placer_AI()
    {
        srand(time(nullptr));
        std::vector<Ship>* ships = &playerbtfs[curr_player].ships;
        while(!is_correct_positioning(curr_player, true))
            for(uint64_t i = 0; i < ships->size(); ++i) {
                if(rand() % 100 < 90) {
                    ships->at(i).x =
                      rand() % playerbtfs[curr_player].matrix.size();
                    ships->at(i).y =
                      rand() % playerbtfs[curr_player].matrix.size();
                }
                if(rand() % 100 < 50) {
                    playerbtfs[curr_player].rotate_ship(i);
                }
            }
        return { '!', 0, 0, 0 };
    }

    Input fighter_AI()
    {
        Input buffer;
        print_frame();
        buffer.optype = 'f';
        uint8_t chance = rand() % 100;
        std::vector<Ship>& ships = playerbtfs[curr_enemy()].ships;
        if(chance < difficulty) {
            uint64_t ship_n = 0;
            for(; ship_n < ships.size(); ++ship_n)
                if(!ships[ship_n].is_killed())
                    break;

            for(uint64_t i = 0; i < ships[ship_n].cells.size(); ++i) {
                if(playerbtfs[curr_enemy()]
                     .matrix[ships[ship_n].x + i * (ships[ship_n].dir == 'h')]
                            [ships[ship_n].y +
                             i * (ships[ship_n].dir == 'v')]) {
                    playerbtfs[curr_enemy()].punch_cell(
                      ships[ship_n].x + i * (ships[ship_n].dir == 'h'),
                      ships[ship_n].y + i * (ships[ship_n].dir == 'v'));
                    if(ships[ship_n].is_killed())
                        eject_killed_ship_domain(curr_enemy(), ships[ship_n]);
                    if(chance >= difficulty / 3)
                        break;
                }
            }
        }
        buffer.x = rand() % playerbtfs[curr_enemy()].matrix.size();
        buffer.y = rand() % playerbtfs[curr_enemy()].matrix.size();

        return buffer;
    }
    Input receive_input(uint64_t player_n) // TODO: improve code
    {
        Input buffer;
        if(players[player_n].is_human) {
            do {
                if(std::cin.fail()) {
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(),
                                    '\n');
                }
                if(phase == "placing") {
                    std::cin >> buffer.sid;
                    std::cin >> buffer.optype;
                    if(buffer.optype == 'p') {
                        std::cin >> buffer.x;
                        std::cin >> buffer.y;
                    }
                } else {
                    buffer.optype = 'f';
                    std::cin >> buffer.x;
                    std::cin >> buffer.y;
                }
            } while(std::cin.fail());
        } else {
            if(phase == "placing") {
                buffer = placer_AI(); // difficulty, playerbtfs, player_n);
                // p_count)
            } else {
                buffer = fighter_AI(); // difficulty, playerbtfs, player_n);
            }
        }
        return buffer;
    }
    void players_place_ships()
    {
        phase = "placing";
        bool is_successful = false;
        curr_player = 0;
        print_frame();
        Input action;
        for(; curr_player < players.size(); ++curr_player) {

            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            regenerate_screen();
            action = receive_input(curr_player);
            while(action.optype != '!' ||
                  !is_correct_positioning(curr_player)) {

                info_message.clear();
                if(action.optype == 'a') {
                    placer_AI(); // autoplace ships
                } else if(action.optype == 'p') {
                    is_successful = playerbtfs[curr_player].place_ship(
                      action.sid, action.x, action.y);
                } else if(action.optype == 'r') {
                    is_successful =
                      playerbtfs[curr_player].rotate_ship(action.sid);
                } else if(action.optype != '!') {
                    info_message = "Unknown operation! ";
                }
                if(is_successful)
                    info_message += "Success!";
                else
                    info_message += "Error!  ";
                print_frame();
                action = receive_input(curr_player);
            }
        }
        curr_player = 0;
    }

    void eject_killed_ship_domain(uint64_t player_n, Ship& i)
    {
        std::cout << i.dir;
        uint64_t xend =
          i.x + 1 + (i.cells.size()) * (i.dir == 'h') + (i.dir == 'v');
        uint64_t yend =
          i.y + 1 + (i.cells.size()) * (i.dir == 'v') + (i.dir == 'h');
        for(uint64_t x = i.x - (i.x != 0); x < xend; ++x) {
            for(uint64_t y = i.y - (i.y != 0); y < yend; ++y) {
                // std::cout << x << "of" << xend << "," << y << "of " << yend
                //          << std::endl;
                if((x < playerbtfs[player_n].matrix.size()) &&
                   (y < playerbtfs[player_n].matrix.size())) {
                    playerbtfs[player_n].matrix[x][y] = false;
                }
            }
        }
    }
    uint64_t curr_enemy()
    {
        return (curr_player + 1) % p_count;
    }

    void battle_loop()
    {
        phase = "shooting";
        Input action;
        uint8_t result = 0;
        while(!is_game_end()) {
            print_frame();
            action = receive_input(curr_player);
            result = playerbtfs[curr_enemy()].punch_cell(action.x, action.y);
            if(result != 0) {
                if(result == 1) {
                    info_message = "Lost time&shots";
                } else if(result == 2) {
                    info_message = "Damaged a ship!";
                } else {
                    info_message = "Killed #" + std::to_string(result - 3);
                    eject_killed_ship_domain(
                      curr_enemy(), playerbtfs[curr_enemy()].ships[result - 3]);
                }
            } else {
                info_message = "Error!  ";
            }
            if(result == 1) {
                curr_player = curr_enemy();
                regenerate_screen();
            }
        }
        if(is_defeated(curr_player)) {
            curr_player = curr_enemy();
        }
        std::cout << std::endl
                  << std::endl
                  << players[curr_player].name << " wins!" << std::endl
                  << std::endl;
        if(players[curr_player].is_human) {
            std::cout << "WOW! My AI is defeated..." << std::endl;
            std::cout << "BUT, have you tried to increase difficulty?"
                      << std::endl;
        } else {
            std::cout << "Ha-ha! Humans can't beat my AI!" << std::endl;
            std::cout << "Maybe you want to decrease its diffuiculty?"
                      << std::endl;
        }
    }

    void main_loop()
    {
        std::string action = "help";
        while(action != "exit") {
            if(action == "help") {
                std::cout << GAME_HELP << std::endl;
            } else if(action == "play") {
                players_place_ships();
                battle_loop();
            } else if(action == "conf") {
                configure();
            }
            std::cout << MAIN_HELP << std::endl;
            std::cin >> action;
        }
    }
};

int
main()
{
    App app;
    app.main_loop();
    return 0;
}
