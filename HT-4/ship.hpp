// Copyright 2021 Fe-Ti <btm.007@mail.ru>
#ifndef BATTLE_SHIP
#define BATTLE_SHIP

#include <cstdint>
#include <vector>

class Ship
{
  public:
    std::vector<bool> cells;
    uint64_t x;
    uint64_t y;
    char dir; // direction

    Ship(uint64_t size, uint64_t x, uint64_t y, char dir);

    // unprotected functions
    void rotate(); // toggle rotation (simply swap directional coordinates)
    void punch_me(uint64_t cell_id);
    bool is_killed();
    
    Ship operator=(Ship& other_ship);
};

#endif