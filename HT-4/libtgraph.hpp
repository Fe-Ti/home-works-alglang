// Copyright 2021 Fe-Ti <btm.007@mail.ru>

#ifndef TEXTSCREEN
#define TEXTSCREEN

#include <iostream>
#include <string>
#include <vector>

class Screen
{

    std::vector<std::vector<char>> buffer;

  public:
    Screen();
    Screen(uint64_t res_x, uint64_t res_y);
    void resize(uint64_t res_x, uint64_t res_y);
    void clear();
    char& raw_buffer(uint64_t pos_x, uint64_t pos_y);
    uint64_t xsize() const;
    uint64_t ysize() const;

    void print(std::ostream& out = std::cout);

    void place_text(uint64_t pos_x,
                    uint64_t pos_y,
                    const std::string& text,
                    bool horizontal = true);
    void rel_place_text(float rel_x,
                        float rel_y,
                        const std::string& text,
                        bool horizontal = true);
    void rel_place_text(double rel_x,
                        double rel_y,
                        const std::string& text,
                        bool horizontal = true);

    void place_matrix(uint64_t pos_x,
                      uint64_t pos_y,
                      const std::vector<std::vector<char>>& matrix); /*
     void place_matrix(uint64_t pos_x,
                       uint64_t pos_y,
                       const std::vector<std::vector<bool>>& matrix);*/
    void rel_place_matrix(float rel_x,
                          float rel_y,
                          const std::vector<std::vector<char>>& matrix);
    void rel_place_matrix(double rel_x,
                          double rel_y,
                          const std::vector<std::vector<char>>& matrix);
                          
                          ////
};

#endif