// Copyright 2020 Fe-Ti <btm.007@mail.ru>

#include <string>
#include <vector>

#ifndef CONSTANTS_H
#define CONSTANTS_H

// constant values for easier understanding what's going on in the code
const char wspace = ' ';
const char separator = ',';
const char eqchar = '=';

const char orp = '(';
const char crp = ')';
const char osp = '[';
const char csp = ']';
const char ofp = '<'; // functions' parentheses
const char cfp = '>'; // before replacing with corresponding expression
const char oap = '{'; // and
const char cap = '}'; // after doing this

const char opow = '^';
const char omul = '*';
const char odiv = '/';
const char osum = '+';
const char osub = '-';
const std::vector<std::vector<char>> ops = { { opow, opow },
                                             { omul, odiv },
                                             { osum, osub } };

const std::vector<char> prohibited_chars = { ofp, cfp, osp,      csp,
                                             oap, cap, separator };
// separator should not be prohibited, but I have no easy-to-realize idea
// how to solve situations like 
// 
// parse(1+2+3-(123,190-1000)*999)
//  |
//  V 
// parse(123,190-1000)
// calculate(123,190-1000)
//  |
//  V
// calculate(1+2+3-123,810*999)
//  <BOOM!> we have wrong answer.
//
// Also I need to hurry up with this project and temporarily forget about this  

const std::vector<std::string> builtins = { "sin", "cos", "tg", "ctg" };

#endif
