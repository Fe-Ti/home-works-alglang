// Copyright 2020 Fe-Ti <btm.007@mail.ru>

#include <algorithm>
#include <cmath>
#include <exception>
#include <iostream>
#include <string>
#include <vector>

#include "calctypes.h"
#include "constants.h"
#include "split.h"

template<typename T>
bool
is_in(T ch, const std::vector<T>& vec)
{
    if(std::find(vec.begin(), vec.end(), ch) != vec.end()) {
        return true;
    }
    return false;
}

bool
is_in(char ch, const std::vector<std::vector<char>>& vecvec)
{
    for(const std::vector<char>& i : vecvec) {
        if(is_in(ch, i)) {
            return true;
        }
    }
    return false;
}

bool
is_number(std::string& str)
{
    if(str.empty() || (str.size() == 1 && !std::isdigit(str[0])))
        return false;
    int8_t dots_counter = 0;
    int8_t pos = 0;
    for(char& i : str) {
        if(i == '.') {
            ++dots_counter;
        }
        if(!(std::isdigit(i) || (i == '.' && dots_counter < 2))) {
            if(!(i == osub && pos == 0))
                return false;
        }
        ++pos;
    }
    return true;
}

Str_operator
find_closest_operator(std::string& aexp, uint64_t pos) // returns the closest
                                                       // left operator
{
    Str_operator op = { '\0', aexp.size() };
    uint64_t opos;
    for(std::vector<char> i : ops) {
        for(char j : i) {
            opos = aexp.find(j, pos);
            if(opos < op.pos && opos != std::string::npos) {
                op.pos = opos;
                op.kind = j;
            }
        }
    }
    return op;
}

Str_operator
rfind_closest_operator(std::string& aexp, uint64_t pos) // returns the closest
                                                        // right operator
{
    Str_operator op = { '\0', aexp.size() };
    uint64_t opos;
    for(std::vector<char> i : ops) {
        for(char j : i) {
            opos = aexp.rfind(j, pos);
            if((opos > op.pos || (opos < op.pos && op.pos == aexp.size())) &&
               opos != std::string::npos) {
                op.pos = opos;
                op.kind = j;
            }
        }
    }
    return op;
}

std::string
get_fid(std::string& aexp,
        uint64_t& op_pos,
        uint64_t& cp_pos,
        uint64_t& fid_pos,
        const char ofp = '<', // functions' parentheses
        const char cfp = '>')
{
    op_pos = aexp.rfind(ofp, cp_pos);

    fid_pos = rfind_closest_operator(aexp, op_pos).pos;
    if(fid_pos == aexp.size()) {
        fid_pos = std::string::npos; // according to c++ documentation
                                     // npos equals -1 in this case
    }

    if(fid_pos == std::string::npos ||
       (fid_pos < aexp.rfind(ofp, op_pos - 1) &&
        aexp.rfind(ofp, op_pos - 1) != std::string::npos)) {

        fid_pos = aexp.rfind(ofp, op_pos - 1);
    }
    ++fid_pos; // increasing fid_pos to match the beginning of the identifier
    //  if there is no operators before '<', then look from the beginning
    return aexp.substr(fid_pos, op_pos - fid_pos);
}

void
calculate_funcs(std::string& aexp)
// builtin functions processor (sin,cos,tg,ctg)
{
    uint64_t fid_pos;
    uint64_t oap_pos;
    uint64_t cap_pos = aexp.find(cap);

    std::string fid;
    std::string buffer;

    float fbuffer;

    while(cap_pos != std::string::npos) {
        fid = get_fid(aexp, oap_pos, cap_pos, fid_pos, oap, cap);
        // getting fid, ofp_pos & fid_pos
        buffer = aexp.substr(oap_pos + 1, cap_pos - oap_pos - 1);
        if(is_number(buffer)) {
            fbuffer = std::stof(buffer);
            if(fid == builtins[0]) { // sin as present in constants.h
                fbuffer = std::sin(fbuffer);
            } else if(fid == builtins[1]) { // cos
                fbuffer = std::cos(fbuffer);
            } else if(fid == builtins[2]) { // tg
                fbuffer = std::tan(fbuffer);
            } else if(fid == builtins[3]) { // ctg
                fbuffer = 1. / std::tan(fbuffer);
            }
            buffer = std::to_string(fbuffer);
            aexp.replace(fid_pos, cap_pos - fid_pos + 1, buffer);
        } else {
            std::cout << "Unknown var, skiping" << std::endl;
            aexp.replace(
              aexp.begin() + oap_pos, aexp.begin() + oap_pos + 1, 1, osp);
            aexp.replace(
              aexp.begin() + cap_pos, aexp.begin() + cap_pos + 1, 1, csp);
        }
        cap_pos = aexp.find(cap, cap_pos);
    }
}

void
perform_opcalc(std::string& aexp, uint64_t& pos, const char op)
{
    if(pos == 0 && op == osub) {
        pos = 1;
        return;
    }
    Str_operator lc_op;
    Str_operator rc_op;
    std::string sleft_opd;
    float left_opd;
    std::string sright_opd;
    float right_opd;

    // finding left and right operands with their sign
    lc_op = rfind_closest_operator(aexp, pos - 1);
    if(lc_op.kind == osub) {
        if(lc_op.pos > 0) {
            if(is_in(aexp[lc_op.pos - 1], ops)) {
                lc_op = rfind_closest_operator(aexp, lc_op.pos - 1);
            }
        } else {
            lc_op.kind = '\0';
        }
    }
    if(lc_op.kind == '\0') {
        lc_op.pos = -1;
    }
    rc_op = find_closest_operator(aexp, pos + 1);
    if(rc_op.kind == osub && rc_op.pos - pos == 1) {
        rc_op = find_closest_operator(aexp, rc_op.pos + 1);
    }
    // getting them
    sleft_opd = aexp.substr(lc_op.pos + 1, pos - lc_op.pos - 1);
    sright_opd = aexp.substr(pos + 1, rc_op.pos - pos - 1);
    // checking if they are numbers
    if(is_number(sleft_opd) && is_number(sright_opd)) {
        try {
            left_opd = std::stof(sleft_opd);
            right_opd = std::stof(sright_opd);
        } catch(std::out_of_range& error) {
            throw "One of the operands is way too big.";
        }
        if(op == opow) {
            left_opd = std::pow(left_opd, right_opd);
        } else if(op == omul) {
            left_opd = left_opd * right_opd;
        } else if(op == odiv) {
            left_opd = left_opd / right_opd;
        } else if(op == osum) {
            left_opd = left_opd + right_opd;
        } else if(op == osub) {
            left_opd = left_opd - right_opd;
        }
    } else {
        // if not throw an exception (at least an error message)
        throw "First snow, then silence."
              "\n Error appears in operands"
              "\n So gracefully.";
    }
    sleft_opd = std::to_string(left_opd);

    aexp.replace(lc_op.pos + 1, rc_op.pos - lc_op.pos - 1, sleft_opd);
}

void
calculate_ops(std::string& aexp) // calculator main core
{
    std::vector<uint64_t> pos_vec = { std::string::npos };
    for(const std::vector<char>& i : ops) {
        for(char j : i) {
            pos_vec.push_back(aexp.find(j));
        }
        while(pos_vec[0] != std::string::npos ||
              pos_vec[1] != std::string::npos) {

            if(pos_vec[0] < pos_vec[1] && pos_vec[0] != std::string::npos) {
                perform_opcalc(aexp, pos_vec[0], i[0]);
            } else if(pos_vec[1] <= pos_vec[0] &&
                      pos_vec[1] != std::string::npos) {
                perform_opcalc(aexp, pos_vec[1], i[1]);
            }
            if(pos_vec[0] > 1) {
                pos_vec[0] = 1;
            }
            if(pos_vec[1] > 1) {
                pos_vec[1] = 1;
            }
            pos_vec[0] = aexp.find(i[0], pos_vec[0]);
            pos_vec[1] = aexp.find(i[1], pos_vec[1]);
        }
        pos_vec.clear();
    }
}

std::string
calculate(std::string& aexp)
{
    calculate_funcs(aexp);
    calculate_ops(aexp);
    return std::string(aexp);
}
