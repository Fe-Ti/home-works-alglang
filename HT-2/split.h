// Copyright 2020 Fe-Ti <btm.007@mail.ru>
// Copyright 2020 Fe-Ti <btm.007@mail.ru>

#include <string>
#include <vector>

#ifndef SPLIT_H
#define SPLIT_H

std::vector<std::string>
split(std::string& str, char ch, bool clean_empty = true);

#endif
