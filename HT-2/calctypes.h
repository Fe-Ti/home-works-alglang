// Copyright 2020 Fe-Ti <btm.007@mail.ru>

#include <string>
#include <vector>

#ifndef CALCTYPES_H
#define CALCTYPES_H

// types used in the project

struct Str_operator {
    char kind;
    uint64_t pos;
};

struct Session {
    std::vector<std::pair<std::string, std::string>> vars = { { "p", "3.1415" },
                                                              { "e",
                                                                "2.7182" } };
    std::vector<std::pair<std::string, std::string>> functions = {
        { "sin", "x=sin{x}" },
        { "cos", "x=cos{x}" },
        { "tg", "x=tg{x}" },
        { "ctg", "x=ctg{x}" }
    };               // "func", "param,...,param=expression_with_builtins"
    int8_t mode = 0; // 0 to calculate, 1 to set a var
};

struct Bin_operation {
    std::string overall_sign = "+";
    std::string left_opd_sig = "+";
    std::string left_opd;
    std::string op;
    std::string right_opd_sig = "+";
    std::string right_opd;
};

#endif
