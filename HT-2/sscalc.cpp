// Copyright 2020 Fe-Ti <btm.007@mail.ru>

#include <algorithm>
#include <exception>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

#include "calctypes.h"
#include "constants.h"
#include "libcalculate.h"
#include "split.h"

const std::string help_message =
  "--------------------------------------------------------------------------\n"
  ".--------------------.\n"
  "| Help(common notes) |\n"
  "*--------------------*\n"
  "To view information about variables enter 'help_var'\n"
  "To view information about errors enter 'help_error'\n"
  "\n"
  "This is very basic calculator with few builtin functions.\n"
  "To calculate an arithmetical expression you need to:\n"
  "    1) Type the expression;\n"
  "    2) Press 'Enter'('Return') key.\n"
  "Expression example:\n"
  "tg(cos(p))*ctg(cos(p))+2+3+sin(p)\n"
  "\n"
  "Here are some notes:\n"
  "    Allowed operations:\n"
  "        x^y - calculates x in power y\n"
  "        x/y - calculates x divided by y\n"
  "        x*y - calculates x multiplied by y\n"
  "        x+y - calculates the sum of x and y\n"
  "        x-y - calculates x subtracted with y\n"
  "\n"
  "    Allowed functions:\n"
  "        sin(x) - calculates sinus (x is in radians)\n"
  "        cos(x) - calculates cosinus\n"
  "        tg(x)  - calculates tangent\n"
  "        ctg(x) - calculates cotangent\n"
  "--------------------------------------------------------------------------"
  "\n";

const std::string help_var =
  "--------------------------------------------------------------------------\n"
  ".-----------------.\n"
  "| Help(variables) |\n"
  "*-----------------*\n"
  "\n"
  "To set a variable it's needed to:\n"
  "    1) Type a name (identifier) of your variable;\n"
  "    2) Type '=';\n"
  "    3) Type the value of the variable;\n"
  "    4) Press 'Enter'('Return') key.\n"
  "Then you should see you variable in the list of variables.\n"
  "\n"
  "Notes:\n"
  "Variables can be set to different integer or float values.\n"
  "You can't use expressions directly(e.g. a=p+e will give an error).\n"
  "\n"
  "Example:\n"
  "MyEpicVar=1\n"
  "--------------------------------------------------------------------------"
  "\n";

const std::string help_error =
  "--------------------------------------------------------------------------\n"
  ".--------------.\n"
  "| Help(errors) |\n"
  "*--------------*\n"
  "\n"
  "Several errors can occur, in most cases the calculator throws\n"
  "exceptions with an explanatory strings.\n"
  "\n"
  "You can find some (maybe helpful) notes below.\n"
  "\n"
  "Invalid syntax:"
  "This message indicates that something is wrong with the syntax (e.g. 8+9- "
  "or sun(p) and etc). Usually there is a tip before an error skull.\n"
  "\n"
  "An error in operands:\n"
  "This error is given in haiku form (the idea is borrowed from Haiku OS)\n"
  "and it tells that one of the operands is not a numeric value,\n"
  "e.g. 1+(-(-1)) will give this error, because it will try to calculate --1\n"
  "which is incorrect from the calculator's view point\n"
  "(it considers the first minus to be unary and the second to be binary)\n"
  "--------------------------------------------------------------------------"
  "\n";

const std::string splash = "Enter the expression(or 'help' to view help):\n";

std::ostream&
operator<<(std::ostream& out, Session& session) // output user variables
{
    out << "Currently available variables" << std::endl;
    for(std::pair<std::string, std::string>& i : session.vars) {
        out << i.first << '=' << i.second << ';' << std::endl;
    }
    return out;
}

uint64_t
find(Session& session, std::pair<std::string, std::string>& var)
// find a variable in session var-s
{
    uint64_t counter = 0;
    for(std::pair<std::string, std::string>& i : session.vars) {
        if(i.first == var.first) {
            return counter;
        }
        ++counter;
    }
    return session.vars.size();
}

uint64_t
find(std::vector<std::pair<std::string, std::string>>& sfuncs, std::string& fnc)
{ // find a function in session functions
    uint64_t counter = 0;
    for(std::pair<std::string, std::string>& i : sfuncs) {
        if(i.first == fnc) {
            return counter;
        }
        ++counter;
    }
    return sfuncs.size();
}

bool
is_in(std::string fnc, std::vector<std::pair<std::string, std::string>>& sfuncs)
// an overload for checking if some user defined function is unknown
{
    for(std::pair<std::string, std::string>& i : sfuncs) {
        if(fnc == i.first) {
            return true;
        }
    }
    return false;
}

void
print_error(const char* error) // printing an error if it occurs
{
    std::cout << ".------.    " << std::endl;
    std::cout << "| O  O |    " << std::endl;
    std::cout << "|_ .. _|    " << std::endl;
    std::cout << "  |__|      " << std::endl;
    std::cout << "  OOPS" << std::endl;
    std::cout << error << std::endl << std::endl;
}

void
replace_one_var(std::string& aexp,
                std::string& vid,
                std::string& value,
                const char openingp = orp,
                const char closingp = crp,
                uint64_t cur_pos = 0,
                uint64_t chk_pos0 = 0,
                uint64_t chk_pos1 = 0) // a lot of code for replacing a variable
{                                      // with its value
    cur_pos = aexp.find(vid);
    while(cur_pos != std::string::npos) {
        // while we can find a identifier of that variable in the string:
        // 1) we check if it is the right identifier, or a part of another one
        // 2.0) if yes, replace it
        // 2.1) if no, skip to the next position

        chk_pos0 = rfind_closest_operator(aexp, cur_pos).pos;
        if(chk_pos0 == aexp.size() ||
           (chk_pos0 < aexp.rfind(openingp, cur_pos) &&
            aexp.rfind(openingp, cur_pos) != std::string::npos)) {
            // if we didn't reverse find the closest op.
            // we try to find '(' or openingp
            chk_pos0 = aexp.rfind(openingp, cur_pos);
            if(chk_pos0 < aexp.rfind(separator, cur_pos) &&
               aexp.rfind(separator, cur_pos) != std::string::npos)
                // also there can be a comma so trying to find it
                // and if it is closer than other stuff,
                // we will use its position
                chk_pos0 = aexp.rfind(separator, cur_pos);
        }
        // this operation can be done better
        // if I make rfind closest operator
        // be able to use different set of operators
        // but now the calculator works (suprise) so
        // I do not feel confident enough to do such modifications

        // there is a similar code below
        chk_pos1 = find_closest_operator(aexp, cur_pos).pos;
        if(chk_pos1 > aexp.find(closingp, cur_pos)) {
            chk_pos1 = aexp.find(closingp, cur_pos);
            if(chk_pos1 > aexp.find(separator, cur_pos) &&
               aexp.find(separator, cur_pos) != std::string::npos)
                chk_pos1 = aexp.find(separator, cur_pos);
        }
        // if the substring matches the variable identifier
        // it is replaced by a value given previously
        if(aexp.substr(chk_pos0 + 1, chk_pos1 - chk_pos0 - 1) == vid) {
            aexp.replace(chk_pos0 + 1, chk_pos1 - chk_pos0 - 1, value);
        }
        // finding next possible id
        cur_pos = aexp.find(vid, cur_pos + 1);
    }
}

void
replace_vars(std::string& aexp,
             std::vector<std::pair<std::string, std::string>>& vars)
{ // this is the actual variable replacing used in the prepare fnc
    for(std::pair<std::string, std::string>& i : vars) {
        replace_one_var(aexp, i.first, i.second);
    }
}

void
prepare(std::string& aexp, Session& session) // preparing for analysis
{
    uint64_t chk_pos1 = aexp.find(eqchar);
    std::string save;
    if(chk_pos1 != std::string::npos) {
        if(chk_pos1 > aexp.find(orp) || chk_pos1 > aexp.find(crp)) {
            throw "Functions aren't implemented!"; // currently we can't set
                                                   // user defined functions
        } else {
            session.mode = 1; // but we can change the mode into "add variable"
            save = aexp.substr(0, chk_pos1 + 1);
            aexp.erase(0, chk_pos1 + 1); // preseving the left side of the exp
            std::cout << aexp << std::endl;
        }
    } else {
        session.mode = 0; // if there is no '=' we use "calculate mode"
    }

    replace_vars(aexp, session.vars); // replacing variables

    if(session.mode == 1) {
        aexp.insert(0, save); // returning the left side
                              // which was saved from being replaced
                              // if the variable had already existed
    }
}

bool
has_prohibited_chars(std::string& aexp) // function for checking the expression
{
    for(char i : prohibited_chars) {
        if(aexp.find(i) != std::string::npos) {
            std::cout << "The char " << i << " is prohibited" << std::endl;
            return true;
        }
    }
    return false;
}

bool
has_double_ops(std::string& aexp) // or ops at the beginning oe end of the line
{
    uint64_t cur_pos = 0;
    if(is_in(aexp[cur_pos], ops) && aexp[cur_pos] != osub) {
        return true;
    }
    for(const std::vector<char>& i : ops) {
        for(const char& j : i) {
            cur_pos = aexp.find(j);
            while(cur_pos != std::string::npos) {
                // Fun thing:
                // due to the way how does f.c.o. work we eliminate the case
                // when the last character is an operator
                if(find_closest_operator(aexp, cur_pos + 1).pos - cur_pos < 2) {
                    return true;
                }
                cur_pos = aexp.find(j, cur_pos + 1);
            }
            cur_pos = 0;
        }
    }
    return false;
}

bool
has_missing_args(std::string& aexp)
{
    if(aexp.find("()") != std::string::npos ||
       aexp.find("(,") != std::string::npos ||
       aexp.find(",)") != std::string::npos ||
       aexp.find(",,") != std::string::npos) {
        std::cout << "Oh, I found that there are missing args!" << std::endl;
        return true;
    }
    return false;
}

bool
has_missing_ops(std::string& aexp, Session& session) // a check constructions:
                                                     //  NonOpOrKnownFunc(
                                                     //  )nonoperator

{
    uint64_t cp_pos = aexp.find(crp, 1);
    uint64_t op_pos = aexp.rfind(orp, cp_pos);
    uint64_t fid_pos;
    std::string fid;

    cp_pos = aexp.find(crp, 1);
    while(cp_pos != std::string::npos) {
        fid = get_fid(aexp, op_pos, cp_pos, fid_pos, orp, crp);
        if(op_pos != std::string::npos) {
            if(!is_in(aexp[op_pos - 1], ops)) {
                if(!is_in(fid, session.functions)) {
                    //std::cout << fid << ' ' << op_pos << std::endl;
                    std::cout << "Wind catches lily\n"
                                 " Scatt'ring petals to the wind:\n"
                                 " An unknown function is found."
                              << std::endl;
                    return true;
                }
            }
        }
        if(cp_pos < aexp.size() - 1) {
            if(!is_in(aexp[cp_pos + 1], ops) && aexp[cp_pos + 1] != crp) {
                return true;
            }
        }
        cp_pos = aexp.find(crp, cp_pos + 1);
    }

    return false;
}
void
check_parentheses(std::string& aexp) // checks amount of round par-es
{
    uint64_t orp_count = std::count(aexp.begin(), aexp.end(), orp);
    uint64_t crp_count = std::count(aexp.begin(), aexp.end(), crp);
    if(orp_count > crp_count) {
        throw "There are too less ')'";
    } else if(orp_count < crp_count) {
        throw "There are too less '('";
    }
}
void
analyze(std::string& aexp, Session& session) // static analysis
{
    check_parentheses(aexp);
    if(has_double_ops(aexp) || has_prohibited_chars(aexp) ||
       has_missing_ops(aexp, session) || has_missing_args(aexp)) {
        throw "Invalid syntax";
    }
}

std::string
merge(std::vector<std::string>& args, std::vector<std::string>& ref_expr)
// fills the function with arguments
{ // this is not the best way, but I want to utilize another fnc
    std::vector<std::string> ref_args = split(ref_expr[0], separator);
    for(uint64_t i = 0; i < args.size(); ++i) {
        replace_one_var(ref_expr[1], ref_args[0], args[i], oap, cap);
    }
    return ref_expr[1];
}

void
replace_user_funcs(std::string& aexp, Session& session)
// user defined functions processor
{
    uint64_t cfp_pos = aexp.find(cfp);
    uint64_t ofp_pos;
    uint64_t fib_pos; // function identifier position
    std::string buffer;

    while(cfp_pos != std::string::npos) {

        buffer = get_fid(aexp, ofp_pos, cfp_pos, fib_pos);
        // getting fid, ofp_pos & fib_pos

        buffer = session.functions[find(session.functions, buffer)].second;
        std::vector<std::string> ref_expr = split(buffer, eqchar);

        buffer = aexp.substr(ofp_pos + 1, cfp_pos - ofp_pos - 1);
        std::vector<std::string> args = split(buffer, separator);

        if(args.size() == split(ref_expr[0], separator).size()) {
            buffer = merge(args, ref_expr);
        } else {
            throw "No matching function (args amount error)";
        }
        aexp.replace(fib_pos, cfp_pos - fib_pos + 1, buffer);
        cfp_pos = aexp.find(cfp, cfp_pos + 1);
    }
}

std::string
parse(std::string& aexp, Session& session) // recursively parsing the expression
{
    std::cout << "Parcing " << aexp << std::endl;
    uint64_t crp_pos = aexp.find(crp);
    uint64_t orp_pos = aexp.find(orp);
    std::string calculated_ss;

    while(crp_pos != std::string::npos) {
        orp_pos = aexp.rfind(orp, crp_pos);
        if(orp_pos == std::string::npos) {
            throw "At least one ')' doesn't have a pair ";
        }

        calculated_ss = aexp.substr(orp_pos + 1, crp_pos - orp_pos - 1);
        parse(calculated_ss, session);
        if(orp_pos != 0) {
            if(std::isalpha(aexp[orp_pos - 1])) { // if function
                calculated_ss.insert(0, 1, ofp);  // wrap with <...>
                calculated_ss.push_back(cfp);
            }
        }

        aexp.replace(orp_pos, crp_pos - orp_pos + 1, calculated_ss);

        crp_pos = aexp.find(crp);
    }
    replace_user_funcs(aexp, session);

    calculate(aexp);
    return aexp;
}

void
add_var(std::string& aexp, Session& session) // adds ...=... as a variable
                                             // everything after
                                             // the second '=' and beyond
                                             // is forgotten

{
    std::vector<std::string> spl_aexp = split(aexp, eqchar);
    std::pair<std::string, std::string> new_var = { spl_aexp[0], spl_aexp[1] };
    if(is_number(new_var.first)) {
        throw "The name you've given\n Not allowed being,\n Try another one.";
        // (My apologises for my haiku quality, but I tried my best)
    }
    for(char& i : new_var.first) {
        if(is_in(i, ops) || is_in(i, prohibited_chars)) {
            throw "Prohibited name of a variable (it must be censored)";
        }
    }

    for(char& i : new_var.second) {
        if(!(std::isdigit(i) || i == '.')) {
            throw "Something is wrong with a value, I can't make it float";
            // Yep, it     ~~~~~~~~~~~~~~~~~~
            // totally           O o   o
            // drowns.        |   o  0 o
            //             __\|/___value__@E_
        }
    }
    uint64_t pos = find(session, new_var);
    if(pos != session.vars.size()) {
        session.vars[pos] = new_var;
    } else {
        session.vars.push_back(new_var);
    }
}

int
main(int argc, char* argv[])
{
    Session session;

    std::cout << "Welcome to the Simple Stupid Calculator" << std::endl;
    std::string aexp;
    if(argc > 1) {
        for(int i = 1; i < argc; ++i) {
            aexp += std::string(argv[i]);
        }
    } else {
        std::cout << splash << std::endl;
        std::getline(std::cin, aexp);
    }
    while(aexp != "exit") {
        while(aexp.find(wspace) != std::string::npos) { // eliminating spaces
            aexp.replace(aexp.find(wspace), 1, "");
        }
        if(aexp != "exit") {
            if(aexp == "help") {
                std::cout << help_message;
            } else if(aexp == "help_var") {
                std::cout << help_var;
            } else if(aexp == "help_error") {
                std::cout << help_error;
            } else if(aexp != "") {
                try {
                    prepare(aexp, session);
                    analyze(aexp, session);
                    if(session.mode == 0) {
                        aexp = parse(aexp, session);
                        std::cout << "---------------------" << std::endl;
                        std::cout << "Here is your answer: " << std::endl;
                        std::cout << aexp << std::endl;
                        std::cout << "---------------------" << std::endl;
                    } else if(session.mode == 1) {
                        std::cout << "Setting variable " << aexp << std::endl;
                        add_var(aexp, session);
                    }
                } catch(const char* error) {
                    print_error(error);
                }
            }
            std::cout << session;
            std::cout << splash;
            std::getline(std::cin, aexp);
        }

        if(std::cin.eof()) {
            break;
        }
    }
    std::cout << "Thanks for using the SSCalc!" << std::endl;
    return 0;
}
