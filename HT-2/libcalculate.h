// Copyright 2020 Fe-Ti <btm.007@mail.ru>
// Copyright 2020 Fe-Ti <btm.007@mail.ru>

#include <string>
#include <vector>

#include "calctypes.h"

#ifndef LIBCALCULATE_H
#define LIBCALCULATE_H

template<typename T>
bool
is_in(T ch, const std::vector<T>& vec);

bool
is_in(char ch, const std::vector<std::vector<char>>& vecvec);

bool
is_number(std::string& str);

Str_operator
find_closest_operator(std::string& aexp, uint64_t pos);

Str_operator
rfind_closest_operator(std::string& aexp, uint64_t pos);

std::string
get_fid(std::string& aexp,
        uint64_t& op_pos,
        uint64_t& cp_pos,
        uint64_t& fib_pos,
        const char ofp = '<', // functions' parentheses
        const char cfp = '>');

std::string
calculate(std::string& aexp);

#endif
